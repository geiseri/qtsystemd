# qtsystemd
A Qt5 wrapper around systemd

##Install

```
$ qmake qtsystemd.pro
$ make
$ make install
```

The qmake step has an optional argument of PREFIX that 
can be used to install the library to a different location.

To use in your project you can use ```pkg-config``` to add the
 ```qtsystemd``` module. 

##TODO

* Fill in missing marshal types for dbus_types.h
* Name the ```dbus_types.h``` structures to match systemd
  api documentation.  Some structures are not
  clearly defined so looking at the code is
  required.
* Add proper subclass for ```Manager``` to return proper ```Unit``` types instead
  of paths for ```GetUnit```* methods.
* Implicitly set paths for ```Manager```

## Examples
### Systemd objects: Access the dbus interfaces
```cpp
Systemd::Manager *mgr = new Systemd::Manager(QDBusConnection::systemBus());

Systemd::Service *service = new Systemd::Service(
    // Get the Manager specific DBus interface
    mgr->interface<org::freedesktop::systemd1::Manager>()->GetUnit(QLatin1String("MyUnit.service")),
    // Use the same session as the manager
    mgr->session());

if (
     // Use the Unit specific dbus interface for the Service object
     service->interface<org::freedesktop::systemd1::Unit>()->activeState() == QLatin1String("active") ) {
     connect(
         // Get the dbus specific interface for the Service dbus interface
         service->interface<org::freedesktop::dbus1::Properties>(),
         SIGNAL(PropertiesChanged(QString,QVariantMap,QStringList)),
         this,
         // Due to the async nature here it is advised to use the QVariantMap of
         // property values instead of the dbus interface in the slot.
         SLOT(handlePropertiesChanged(QString,QVariantMap,QStringList)));
}
```
### StateTracker: Track systemd unit lifetime
```cpp
Systemd::Manager *mgr = new Systemd::Manager(QDBusConnection::sessionBus());

StateTracker *tracker = new StateTracker(mgr);

// Unit "loaded" but not running yet
connect(tracker, SIGNAL(unitLoaded()), this, SLOT(handleUnitLoaded()));
// Unit is now in "active" state
connect(tracker, SIGNAL(unitStarted()), this, SLOT(handleUnitStarted()));
// Unit is now in "inactive" state but still loaded
connect(tracker, SIGNAL(unitStopped()), this, SLOT(handleUnitStopped()));
// Unit is now "dead"
connect(tracker, SIGNAL(unitRemoved()), this, SLOT(handleUnitRemoved()));

// Track the unit
tracker->track(QLatin1String("my_unit.service"));

// Optionally the unit can be started manually if it has not
// been started yet
tracker->start();
```


## License

>
 This program is free software: you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation, either
 version 2.1 of the License, or (at your option) any later version.
>
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
>
 You should have received a copy of the GNU Lesser General Public
 License along with this program. If not, see <http://www.gnu.org/licenses/>.

