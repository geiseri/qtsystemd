 # qtsystemd - A Qt5 wrapper to the systemd/logind/machine API.
 # Copyright (C) 2015 Ian Reinhart Geiser
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU Lesser General Public License as
 # published by the Free Software Foundation, either version 2.1 of the
 # License, or (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 # GNU Lesser General Public License for more details.
 #
 # You should have received a copy of the GNU Lesser General Public License
 # along with this program. If not, see <http://www.gnu.org/licenses/>.

QT       += dbus
QT       -= gui

TARGET = tst_scopeprocess_test
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_scopeprocess_test.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

LIBS += -L$${OUT_PWD}/../systemd/ -lqtsystemd
QMAKE_RPATHDIR += $${OUT_PWD}/../systemd

INCLUDEPATH += $$TOP_SRCDIR/systemd $$TOP_OUTDIR/systemd/dbus $$TOP_SRCDIR/systemd/dbus $$TOP_SRCDIR/systemd/adaptors
DEPENDPATH += $$TOP_SRCDIR/systemd

