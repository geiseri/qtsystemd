/*
 * qtsystemd - A Qt5 wrapper to the systemd/logind/machine API.
 * Copyright (C) 2015 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <QString>
#include <QCoreApplication>
#include <QEventLoop>
#include <QTimer>
#include "scoperunner.h"
#include "systemdbus.h"
#include <iostream>

void testStartScope()
{
    QDBusConnection conn = SystemdBus::userBus();

    ScopeRunner runner(conn);

    QList<QByteArray> args;
    args << "systemd-cgls";
    args << "-al";

    runner.setArguments(args);
    runner.setScopeName("test_scope");

    QObject::connect( &runner,
                      &ScopeRunner::receivedStderr,
                      [](QByteArray data){
        std::cerr << data.data();
    });

    QObject::connect( &runner,
                      &ScopeRunner::receivedStdout,
                      [](QByteArray data){
        std::cout << data.data();
    });

    QObject::connect( &runner,
                      &ScopeRunner::scopeReady,
                      [](QSharedPointer<Systemd::Scope> scope){
        qDebug() << "ready" << scope->path();
    });


    runner.start(ScopeRunner::NotifyOnExit,ScopeRunner::AllOutput);
    runner.resume();

    qDebug() << runner.waitForScopeStarted(10000);

    QEventLoop loop;
    QTimer timer;
    QObject::connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));
    timer.start(1000);

    qDebug() << runner.waitForScopeStopped(10000);

}

int main( int argc, char **argv )
{
    QCoreApplication app(argc,argv);

    testStartScope();
    return 0;
}
