/*
 * qtsystemd - A Qt5 wrapper to the systemd/logind/machine API.
 * Copyright (C) 2015 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOGIND_USER_H
#define LOGIND_USER_H

#include <QDBusConnection>
#include <logind_interfaces/logind_types.h>
#include <dbusproperties.h>
#include <dbus_interfaces/properties_interface.h>
#include <logind_interfaces/user_interface.h>

typedef DBusMixinAdaptor<
    DBusMixin <org::freedesktop::login1::User,
        DBusMixin <org::freedesktop::DBus::Properties> > > LoginUserAdaptor;


namespace Logind {
    class User : public LoginUserAdaptor
    {
    public:
        explicit User(const QString &path, const QDBusConnection &conn = QDBusConnection::systemBus());
        ~User();

    };
}


#endif // LOGIND_USER_H
