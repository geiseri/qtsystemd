/*
 * qtsystemd - A Qt5 wrapper to the systemd/logind/machine API.
 * Copyright (C) 2015 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SYSTEMD_SLICE_H
#define SYSTEMD_SLICE_H

#include <QDBusConnection>
#include <systemd/unit.h>
#include <dbus_interfaces/properties_interface.h>
#include <systemd_interfaces/systemd_types.h>
#include <systemd_interfaces/unit_interface.h>
#include <systemd_interfaces/slice_interface.h>

typedef DBusMixinAdaptor<
    DBusMixin <org::freedesktop::systemd1::Slice,
        DBusMixin <org::freedesktop::systemd1::Unit,
            DBusMixin <org::freedesktop::DBus::Properties> > > > SliceAdaptor;

namespace Systemd {
    class Slice : public SliceAdaptor
    {
    public:
        explicit Slice(const QString &path, const QDBusConnection &conn = QDBusConnection::sessionBus());
        ~Slice();

    };
}

#endif // SYSTEMD_SLICE_H
