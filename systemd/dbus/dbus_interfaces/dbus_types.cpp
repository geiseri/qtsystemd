
#include "dbus_types.h"
#include <QDBusVariant>
#include <QDBusArgument>
#include <QDBusMetaType>

namespace dbus_types {

    template <typename K, typename V>
    void map2dbus(QDBusArgument *argument, const QMap<K,V> &message)
    {
        argument->beginMap( qMetaTypeId<K>(), qMetaTypeId<V>()  );
        foreach( K key, message.keys() ) {
            argument->beginMapEntry();
            (*argument) << key << message[key];
            argument->endMapEntry();
        }
        argument->endMap();
    }


    template <typename K, typename V>
    void dbus2map(const QDBusArgument *argument, QMap<K,V> &message)
    {
        argument->beginMap();
        message.clear();
        while ( !argument->atEnd() ) {
            K key;
            V value;
            argument->beginMapEntry();
            (*argument) >> key >> value;
            argument->endMapEntry();
            message[key] = value;
        }
        argument->endMap();
    }


    // This is our base-case for the print function:
    void struct2dbus(QDBusArgument *argument, const QVariant& msg)
    {
      (*argument) << QDBusVariant(msg);
    }

    template <class T>
    void struct2dbus(QDBusArgument *argument, const T& msg)
    {
      (*argument) << msg;
    }

    template <class T>
    void dbus2struct( const QDBusArgument *argument, T& msg)
    {
      (*argument) >> msg;
    }

    // And this is the recursive case:
    template <class A, class... B>
    void struct2dbus( QDBusArgument *argument, const A &head, B... tail)
    {
        struct2dbus(argument, head);
        struct2dbus(argument, tail...);
    }

    template <class A, class... B>
    void dbus2struct( const QDBusArgument *argument, A& head, B&... tail)
    {
        dbus2struct(argument, head);
        dbus2struct(argument, tail...);
    }
}

static int s_dbus_types_typeids[] = {

    
    qDBusRegisterMetaType<ByteArrayMap>(),
    
    qDBusRegisterMetaType<LongLongMap>(),
    
    qDBusRegisterMetaType<QStringMap>(),
    
    qDBusRegisterMetaType<BooleanMap>(),
    
    qDBusRegisterMetaType<IntList>(),

    
    qRegisterMetaType<ByteArrayMap>("ByteArrayMap"),
    
    qRegisterMetaType<LongLongMap>("LongLongMap"),
    
    qRegisterMetaType<QStringMap>("QStringMap"),
    
    qRegisterMetaType<BooleanMap>("BooleanMap"),
    
    qRegisterMetaType<IntList>("IntList"),

};

QDBusArgument &operator<<(QDBusArgument &argument, const ByteArrayMap &message)
{
    dbus_types::map2dbus<QString,QByteArray>(&argument,message);
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, ByteArrayMap &message)
{
    dbus_types::dbus2map<QString,QByteArray>(&argument,message);
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const LongLongMap &message)
{
    dbus_types::map2dbus<QString,qint64>(&argument,message);
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, LongLongMap &message)
{
    dbus_types::dbus2map<QString,qint64>(&argument,message);
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const QStringMap &message)
{
    dbus_types::map2dbus<QString,QString>(&argument,message);
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, QStringMap &message)
{
    dbus_types::dbus2map<QString,QString>(&argument,message);
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const BooleanMap &message)
{
    dbus_types::map2dbus<QString,bool>(&argument,message);
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, BooleanMap &message)
{
    dbus_types::dbus2map<QString,bool>(&argument,message);
    return argument;
}


