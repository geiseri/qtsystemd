
#ifndef DBUS_TYPES_DBUS_TYPES_H
#define DBUS_TYPES_DBUS_TYPES_H

#include <QString>
#include <QVariant>
#include <QDBusArgument>



typedef QMap<QString,QByteArray> ByteArrayMap;
typedef QList<ByteArrayMap> ByteArrayMapList;
QDBusArgument &operator<<(QDBusArgument &argument, const ByteArrayMap &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, ByteArrayMap &message);

Q_DECLARE_METATYPE(ByteArrayMap)

typedef QMap<QString,qint64> LongLongMap;
typedef QList<LongLongMap> LongLongMapList;
QDBusArgument &operator<<(QDBusArgument &argument, const LongLongMap &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, LongLongMap &message);

Q_DECLARE_METATYPE(LongLongMap)

typedef QMap<QString,QString> QStringMap;
typedef QList<QStringMap> QStringMapList;
QDBusArgument &operator<<(QDBusArgument &argument, const QStringMap &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, QStringMap &message);

Q_DECLARE_METATYPE(QStringMap)

typedef QMap<QString,bool> BooleanMap;
typedef QList<BooleanMap> BooleanMapList;
QDBusArgument &operator<<(QDBusArgument &argument, const BooleanMap &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, BooleanMap &message);

Q_DECLARE_METATYPE(BooleanMap)

typedef QList<int> IntList;
Q_DECLARE_METATYPE(IntList)

#endif // DBUS_TYPES_DBUS_TYPES_H

