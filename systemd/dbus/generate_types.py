#!/bin/env python
import optparse
import simplejson
import pystache
import os
import sys

header_template = """
#ifndef {{header_define}}_DBUS_TYPES_H
#define {{header_define}}_DBUS_TYPES_H

#include <QString>
#include <QVariant>
#include <QDBusArgument>

{{#structs}}
struct {{name}} {
{{#members}}    {{type}} {{name}};
    {{/members}}
};

typedef QList<{{name}}> {{name}}List;

QDBusArgument &operator<<(QDBusArgument &argument, const {{name}} &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, {{name}} &message);

Q_DECLARE_METATYPE({{name}})

{{/structs}}

{{#maps}}

typedef QMap<{{key}},{{value}}> {{name}};
typedef QList<{{name}}> {{name}}List;
QDBusArgument &operator<<(QDBusArgument &argument, const {{name}} &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, {{name}} &message);

Q_DECLARE_METATYPE({{name}})
{{/maps}}

{{#arrays}}
typedef QList<{{type}}> {{name}};
Q_DECLARE_METATYPE({{name}})
{{/arrays}}

#endif // {{header_define}}_DBUS_TYPES_H

"""

cpp_template = """
#include "{{namespace}}.h"
#include <QDBusVariant>
#include <QDBusArgument>
#include <QDBusMetaType>

namespace {{namespace}} {

    template <typename K, typename V>
    void map2dbus(QDBusArgument *argument, const QMap<K,V> &message)
    {
        argument->beginMap( qMetaTypeId<K>(), qMetaTypeId<V>()  );
        foreach( K key, message.keys() ) {
            argument->beginMapEntry();
            (*argument) << key << message[key];
            argument->endMapEntry();
        }
        argument->endMap();
    }


    template <typename K, typename V>
    void dbus2map(const QDBusArgument *argument, QMap<K,V> &message)
    {
        argument->beginMap();
        message.clear();
        while ( !argument->atEnd() ) {
            K key;
            V value;
            argument->beginMapEntry();
            (*argument) >> key >> value;
            argument->endMapEntry();
            message[key] = value;
        }
        argument->endMap();
    }


    // This is our base-case for the print function:
    void struct2dbus(QDBusArgument *argument, const QVariant& msg)
    {
      (*argument) << QDBusVariant(msg);
    }

    template <class T>
    void struct2dbus(QDBusArgument *argument, const T& msg)
    {
      (*argument) << msg;
    }

    template <class T>
    void dbus2struct( const QDBusArgument *argument, T& msg)
    {
      (*argument) >> msg;
    }

    // And this is the recursive case:
    template <class A, class... B>
    void struct2dbus( QDBusArgument *argument, const A &head, B... tail)
    {
        struct2dbus(argument, head);
        struct2dbus(argument, tail...);
    }

    template <class A, class... B>
    void dbus2struct( const QDBusArgument *argument, A& head, B&... tail)
    {
        dbus2struct(argument, head);
        dbus2struct(argument, tail...);
    }
}

static int s_{{namespace}}_typeids[] = {

    {{#structs}}qDBusRegisterMetaType<{{name}}>(),
    qDBusRegisterMetaType<{{name}}List>(),
    {{/structs}}{{#maps}}
    qDBusRegisterMetaType<{{name}}>(),
    {{/maps}}{{#arrays}}
    qDBusRegisterMetaType<{{name}}>(),
    {{/arrays}}

    {{#structs}}qRegisterMetaType<{{name}}>("{{name}}"),
    qRegisterMetaType<{{name}}List>("{{name}}List"),
    {{/structs}}{{#maps}}
    qRegisterMetaType<{{name}}>("{{name}}"),
    {{/maps}}{{#arrays}}
    qRegisterMetaType<{{name}}>("{{name}}"),
    {{/arrays}}

};

{{#maps}}
QDBusArgument &operator<<(QDBusArgument &argument, const {{name}} &message)
{
    {{namespace}}::map2dbus<{{key}},{{value}}>(&argument,message);
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, {{name}} &message)
{
    {{namespace}}::dbus2map<{{key}},{{value}}>(&argument,message);
    return argument;
}
{{/maps}}

{{#structs}}
QDBusArgument &operator<<(QDBusArgument &argument, const {{name}} &message)
{
    argument.beginStructure();
    {{namespace}}::struct2dbus(&argument{{#members}},
                message.{{name}}{{/members}});
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, {{name}} &message)
{
    argument.beginStructure();
    {{namespace}}::dbus2struct(&argument{{#members}},
                message.{{name}}{{/members}});
    argument.endStructure();
    return argument;
}
{{/structs}}

"""

class TypeDefinitions(object):
    def __init__(self, jsonFile):
        json = simplejson.load(jsonFile)
        self._namespace = json["namespace"]
        self._maps = []
        self._structs = []
        self._arrays = []
        if "maps" in json:
            self._maps = json["maps"]
        if "structs" in json:
            self._structs = json["structs"]
        if "arrays" in json:
            self._arrays = json["arrays"]

    def namespace(self):
        return self._namespace

    def header_define(self):
        return self._namespace.upper()

    def maps(self):
        return self._maps

    def structs(self):
        return self._structs

    def arrays(self):
        return self._arrays

parser = optparse.OptionParser(usage="usage: %prog [options] input.json",version="%prog 1.0")
parser.add_option("-d", "--header", help="Generate header", action="store_true", default=False)
parser.add_option("-i", "--cpp", help="Generate cpp", action="store_true", default=False)

(options, args) = parser.parse_args()


if len(args) != 1:
    parser.error("wrong number of arguments")
json = open(args[0])

defs = TypeDefinitions(json)
renderer = pystache.Renderer()

hpp_out = open(defs.namespace() + ".h", "w")
cpp_out = open(defs.namespace() + ".cpp", "w")

hpp_out.write(renderer.render(header_template, defs))
cpp_out.write(renderer.render(cpp_template, defs))

hpp_out.close()
cpp_out.close()
