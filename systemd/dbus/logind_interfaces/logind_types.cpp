
#include "logind_types.h"
#include <QDBusVariant>
#include <QDBusArgument>
#include <QDBusMetaType>

namespace logind_types {

    template <typename K, typename V>
    void map2dbus(QDBusArgument *argument, const QMap<K,V> &message)
    {
        argument->beginMap( qMetaTypeId<K>(), qMetaTypeId<V>()  );
        foreach( K key, message.keys() ) {
            argument->beginMapEntry();
            (*argument) << key << message[key];
            argument->endMapEntry();
        }
        argument->endMap();
    }


    template <typename K, typename V>
    void dbus2map(const QDBusArgument *argument, QMap<K,V> &message)
    {
        argument->beginMap();
        message.clear();
        while ( !argument->atEnd() ) {
            K key;
            V value;
            argument->beginMapEntry();
            (*argument) >> key >> value;
            argument->endMapEntry();
            message[key] = value;
        }
        argument->endMap();
    }


    // This is our base-case for the print function:
    void struct2dbus(QDBusArgument *argument, const QVariant& msg)
    {
      (*argument) << QDBusVariant(msg);
    }

    template <class T>
    void struct2dbus(QDBusArgument *argument, const T& msg)
    {
      (*argument) << msg;
    }

    template <class T>
    void dbus2struct( const QDBusArgument *argument, T& msg)
    {
      (*argument) >> msg;
    }

    // And this is the recursive case:
    template <class A, class... B>
    void struct2dbus( QDBusArgument *argument, const A &head, B... tail)
    {
        struct2dbus(argument, head);
        struct2dbus(argument, tail...);
    }

    template <class A, class... B>
    void dbus2struct( const QDBusArgument *argument, A& head, B&... tail)
    {
        dbus2struct(argument, head);
        dbus2struct(argument, tail...);
    }
}

static int s_logind_types_typeids[] = {

    qDBusRegisterMetaType<SessionProperty>(),
    qDBusRegisterMetaType<SessionPropertyList>(),
    qDBusRegisterMetaType<SeatPath>(),
    qDBusRegisterMetaType<SeatPathList>(),
    qDBusRegisterMetaType<SessionPath>(),
    qDBusRegisterMetaType<SessionPathList>(),
    qDBusRegisterMetaType<UserPath>(),
    qDBusRegisterMetaType<UserPathList>(),
    qDBusRegisterMetaType<LogindSession>(),
    qDBusRegisterMetaType<LogindSessionList>(),
    qDBusRegisterMetaType<LogindUser>(),
    qDBusRegisterMetaType<LogindUserList>(),
    qDBusRegisterMetaType<Inhibitor>(),
    qDBusRegisterMetaType<InhibitorList>(),
    qDBusRegisterMetaType<ShutdownMessage>(),
    qDBusRegisterMetaType<ShutdownMessageList>(),
    
    qRegisterMetaType<SessionProperty>("SessionProperty"),
    qRegisterMetaType<SessionPropertyList>("SessionPropertyList"),
    qRegisterMetaType<SeatPath>("SeatPath"),
    qRegisterMetaType<SeatPathList>("SeatPathList"),
    qRegisterMetaType<SessionPath>("SessionPath"),
    qRegisterMetaType<SessionPathList>("SessionPathList"),
    qRegisterMetaType<UserPath>("UserPath"),
    qRegisterMetaType<UserPathList>("UserPathList"),
    qRegisterMetaType<LogindSession>("LogindSession"),
    qRegisterMetaType<LogindSessionList>("LogindSessionList"),
    qRegisterMetaType<LogindUser>("LogindUser"),
    qRegisterMetaType<LogindUserList>("LogindUserList"),
    qRegisterMetaType<Inhibitor>("Inhibitor"),
    qRegisterMetaType<InhibitorList>("InhibitorList"),
    qRegisterMetaType<ShutdownMessage>("ShutdownMessage"),
    qRegisterMetaType<ShutdownMessageList>("ShutdownMessageList"),
    
};


QDBusArgument &operator<<(QDBusArgument &argument, const SessionProperty &message)
{
    argument.beginStructure();
    logind_types::struct2dbus(&argument,
                message.key,
                message.value);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, SessionProperty &message)
{
    argument.beginStructure();
    logind_types::dbus2struct(&argument,
                message.key,
                message.value);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const SeatPath &message)
{
    argument.beginStructure();
    logind_types::struct2dbus(&argument,
                message.name,
                message.path);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, SeatPath &message)
{
    argument.beginStructure();
    logind_types::dbus2struct(&argument,
                message.name,
                message.path);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const SessionPath &message)
{
    argument.beginStructure();
    logind_types::struct2dbus(&argument,
                message.name,
                message.path);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, SessionPath &message)
{
    argument.beginStructure();
    logind_types::dbus2struct(&argument,
                message.name,
                message.path);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const UserPath &message)
{
    argument.beginStructure();
    logind_types::struct2dbus(&argument,
                message.name,
                message.path);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, UserPath &message)
{
    argument.beginStructure();
    logind_types::dbus2struct(&argument,
                message.name,
                message.path);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const LogindSession &message)
{
    argument.beginStructure();
    logind_types::struct2dbus(&argument,
                message.id,
                message.uid,
                message.username,
                message.seat,
                message.path);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, LogindSession &message)
{
    argument.beginStructure();
    logind_types::dbus2struct(&argument,
                message.id,
                message.uid,
                message.username,
                message.seat,
                message.path);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const LogindUser &message)
{
    argument.beginStructure();
    logind_types::struct2dbus(&argument,
                message.id,
                message.name,
                message.path);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, LogindUser &message)
{
    argument.beginStructure();
    logind_types::dbus2struct(&argument,
                message.id,
                message.name,
                message.path);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const Inhibitor &message)
{
    argument.beginStructure();
    logind_types::struct2dbus(&argument,
                message.what,
                message.who,
                message.why,
                message.mode,
                message.uid,
                message.pid);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, Inhibitor &message)
{
    argument.beginStructure();
    logind_types::dbus2struct(&argument,
                message.what,
                message.who,
                message.why,
                message.mode,
                message.uid,
                message.pid);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const ShutdownMessage &message)
{
    argument.beginStructure();
    logind_types::struct2dbus(&argument,
                message.message,
                message.time);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, ShutdownMessage &message)
{
    argument.beginStructure();
    logind_types::dbus2struct(&argument,
                message.message,
                message.time);
    argument.endStructure();
    return argument;
}

