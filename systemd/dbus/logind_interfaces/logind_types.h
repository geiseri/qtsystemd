
#ifndef LOGIND_TYPES_DBUS_TYPES_H
#define LOGIND_TYPES_DBUS_TYPES_H

#include <QString>
#include <QVariant>
#include <QDBusArgument>

struct SessionProperty {
    QString key;
    QVariant value;
};

typedef QList<SessionProperty> SessionPropertyList;

QDBusArgument &operator<<(QDBusArgument &argument, const SessionProperty &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, SessionProperty &message);

Q_DECLARE_METATYPE(SessionProperty)

struct SeatPath {
    QString name;
    QDBusObjectPath path;
};

typedef QList<SeatPath> SeatPathList;

QDBusArgument &operator<<(QDBusArgument &argument, const SeatPath &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, SeatPath &message);

Q_DECLARE_METATYPE(SeatPath)

struct SessionPath {
    QString name;
    QDBusObjectPath path;
};

typedef QList<SessionPath> SessionPathList;

QDBusArgument &operator<<(QDBusArgument &argument, const SessionPath &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, SessionPath &message);

Q_DECLARE_METATYPE(SessionPath)

struct UserPath {
    QString name;
    QDBusObjectPath path;
};

typedef QList<UserPath> UserPathList;

QDBusArgument &operator<<(QDBusArgument &argument, const UserPath &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, UserPath &message);

Q_DECLARE_METATYPE(UserPath)

struct LogindSession {
    QString id;
    quint32 uid;
    QString username;
    QString seat;
    QDBusObjectPath path;
};

typedef QList<LogindSession> LogindSessionList;

QDBusArgument &operator<<(QDBusArgument &argument, const LogindSession &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, LogindSession &message);

Q_DECLARE_METATYPE(LogindSession)

struct LogindUser {
    QString id;
    quint32 name;
    QDBusObjectPath path;
};

typedef QList<LogindUser> LogindUserList;

QDBusArgument &operator<<(QDBusArgument &argument, const LogindUser &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, LogindUser &message);

Q_DECLARE_METATYPE(LogindUser)

struct Inhibitor {
    QString what;
    QString who;
    QString why;
    QString mode;
    quint32 uid;
    quint32 pid;
};

typedef QList<Inhibitor> InhibitorList;

QDBusArgument &operator<<(QDBusArgument &argument, const Inhibitor &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, Inhibitor &message);

Q_DECLARE_METATYPE(Inhibitor)

struct ShutdownMessage {
    QString message;
    quint32 time;
};

typedef QList<ShutdownMessage> ShutdownMessageList;

QDBusArgument &operator<<(QDBusArgument &argument, const ShutdownMessage &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, ShutdownMessage &message);

Q_DECLARE_METATYPE(ShutdownMessage)




#endif // LOGIND_TYPES_DBUS_TYPES_H

