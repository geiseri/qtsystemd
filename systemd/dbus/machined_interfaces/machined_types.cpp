
#include "machined_types.h"
#include <QDBusVariant>
#include <QDBusArgument>
#include <QDBusMetaType>

namespace machined_types {

    template <typename K, typename V>
    void map2dbus(QDBusArgument *argument, const QMap<K,V> &message)
    {
        argument->beginMap( qMetaTypeId<K>(), qMetaTypeId<V>()  );
        foreach( K key, message.keys() ) {
            argument->beginMapEntry();
            (*argument) << key << message[key];
            argument->endMapEntry();
        }
        argument->endMap();
    }


    template <typename K, typename V>
    void dbus2map(const QDBusArgument *argument, QMap<K,V> &message)
    {
        argument->beginMap();
        message.clear();
        while ( !argument->atEnd() ) {
            K key;
            V value;
            argument->beginMapEntry();
            (*argument) >> key >> value;
            argument->endMapEntry();
            message[key] = value;
        }
        argument->endMap();
    }


    // This is our base-case for the print function:
    void struct2dbus(QDBusArgument *argument, const QVariant& msg)
    {
      (*argument) << QDBusVariant(msg);
    }

    template <class T>
    void struct2dbus(QDBusArgument *argument, const T& msg)
    {
      (*argument) << msg;
    }

    template <class T>
    void dbus2struct( const QDBusArgument *argument, T& msg)
    {
      (*argument) >> msg;
    }

    // And this is the recursive case:
    template <class A, class... B>
    void struct2dbus( QDBusArgument *argument, const A &head, B... tail)
    {
        struct2dbus(argument, head);
        struct2dbus(argument, tail...);
    }

    template <class A, class... B>
    void dbus2struct( const QDBusArgument *argument, A& head, B&... tail)
    {
        dbus2struct(argument, head);
        dbus2struct(argument, tail...);
    }
}

static int s_machined_types_typeids[] = {

    qDBusRegisterMetaType<MachineProperty>(),
    qDBusRegisterMetaType<MachinePropertyList>(),
    qDBusRegisterMetaType<MachineAddress>(),
    qDBusRegisterMetaType<MachineAddressList>(),
    qDBusRegisterMetaType<MachineInfo>(),
    qDBusRegisterMetaType<MachineInfoList>(),
    
    qRegisterMetaType<MachineProperty>("MachineProperty"),
    qRegisterMetaType<MachinePropertyList>("MachinePropertyList"),
    qRegisterMetaType<MachineAddress>("MachineAddress"),
    qRegisterMetaType<MachineAddressList>("MachineAddressList"),
    qRegisterMetaType<MachineInfo>("MachineInfo"),
    qRegisterMetaType<MachineInfoList>("MachineInfoList"),
    
};


QDBusArgument &operator<<(QDBusArgument &argument, const MachineProperty &message)
{
    argument.beginStructure();
    machined_types::struct2dbus(&argument,
                message.key,
                message.value);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, MachineProperty &message)
{
    argument.beginStructure();
    machined_types::dbus2struct(&argument,
                message.key,
                message.value);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const MachineAddress &message)
{
    argument.beginStructure();
    machined_types::struct2dbus(&argument,
                message.id,
                message.address);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, MachineAddress &message)
{
    argument.beginStructure();
    machined_types::dbus2struct(&argument,
                message.id,
                message.address);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const MachineInfo &message)
{
    argument.beginStructure();
    machined_types::struct2dbus(&argument,
                message.name,
                message.machineClass,
                message.service,
                message.path);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, MachineInfo &message)
{
    argument.beginStructure();
    machined_types::dbus2struct(&argument,
                message.name,
                message.machineClass,
                message.service,
                message.path);
    argument.endStructure();
    return argument;
}

