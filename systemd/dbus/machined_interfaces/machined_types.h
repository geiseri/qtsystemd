
#ifndef MACHINED_TYPES_DBUS_TYPES_H
#define MACHINED_TYPES_DBUS_TYPES_H

#include <QString>
#include <QVariant>
#include <QDBusArgument>

struct MachineProperty {
    QString key;
    QVariant value;
};

typedef QList<MachineProperty> MachinePropertyList;

QDBusArgument &operator<<(QDBusArgument &argument, const MachineProperty &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, MachineProperty &message);

Q_DECLARE_METATYPE(MachineProperty)

struct MachineAddress {
    int id;
    QByteArray address;
};

typedef QList<MachineAddress> MachineAddressList;

QDBusArgument &operator<<(QDBusArgument &argument, const MachineAddress &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, MachineAddress &message);

Q_DECLARE_METATYPE(MachineAddress)

struct MachineInfo {
    QString name;
    QString machineClass;
    QString service;
    QDBusObjectPath path;
};

typedef QList<MachineInfo> MachineInfoList;

QDBusArgument &operator<<(QDBusArgument &argument, const MachineInfo &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, MachineInfo &message);

Q_DECLARE_METATYPE(MachineInfo)




#endif // MACHINED_TYPES_DBUS_TYPES_H

