
#include "networkd_types.h"
#include <QDBusVariant>
#include <QDBusArgument>
#include <QDBusMetaType>

namespace networkd_types {

    template <typename K, typename V>
    void map2dbus(QDBusArgument *argument, const QMap<K,V> &message)
    {
        argument->beginMap( qMetaTypeId<K>(), qMetaTypeId<V>()  );
        foreach( K key, message.keys() ) {
            argument->beginMapEntry();
            (*argument) << key << message[key];
            argument->endMapEntry();
        }
        argument->endMap();
    }


    template <typename K, typename V>
    void dbus2map(const QDBusArgument *argument, QMap<K,V> &message)
    {
        argument->beginMap();
        message.clear();
        while ( !argument->atEnd() ) {
            K key;
            V value;
            argument->beginMapEntry();
            (*argument) >> key >> value;
            argument->endMapEntry();
            message[key] = value;
        }
        argument->endMap();
    }


    // This is our base-case for the print function:
    void struct2dbus(QDBusArgument *argument, const QVariant& msg)
    {
      (*argument) << QDBusVariant(msg);
    }

    template <class T>
    void struct2dbus(QDBusArgument *argument, const T& msg)
    {
      (*argument) << msg;
    }

    template <class T>
    void dbus2struct( const QDBusArgument *argument, T& msg)
    {
      (*argument) >> msg;
    }

    // And this is the recursive case:
    template <class A, class... B>
    void struct2dbus( QDBusArgument *argument, const A &head, B... tail)
    {
        struct2dbus(argument, head);
        struct2dbus(argument, tail...);
    }

    template <class A, class... B>
    void dbus2struct( const QDBusArgument *argument, A& head, B&... tail)
    {
        dbus2struct(argument, head);
        dbus2struct(argument, tail...);
    }
}

static int s_networkd_types_typeids[] = {

    qDBusRegisterMetaType<DnsEntry>(),
    qDBusRegisterMetaType<DnsEntryList>(),
    qDBusRegisterMetaType<DomainEntry>(),
    qDBusRegisterMetaType<DomainEntryList>(),
    qDBusRegisterMetaType<ResolverTransactionStatistics>(),
    qDBusRegisterMetaType<ResolverTransactionStatisticsList>(),
    qDBusRegisterMetaType<ResolverCacheStatistics>(),
    qDBusRegisterMetaType<ResolverCacheStatisticsList>(),
    qDBusRegisterMetaType<ResolverDNSSECStatistics>(),
    qDBusRegisterMetaType<ResolverDNSSECStatisticsList>(),
    qDBusRegisterMetaType<HostnameAddress>(),
    qDBusRegisterMetaType<HostnameAddressList>(),
    qDBusRegisterMetaType<ResolveAddressName>(),
    qDBusRegisterMetaType<ResolveAddressNameList>(),
    qDBusRegisterMetaType<ResolveRecord>(),
    qDBusRegisterMetaType<ResolveRecordList>(),
    qDBusRegisterMetaType<ResolveServiceSrvData>(),
    qDBusRegisterMetaType<ResolveServiceSrvDataList>(),
    qDBusRegisterMetaType<LinkDns>(),
    qDBusRegisterMetaType<LinkDnsList>(),
    qDBusRegisterMetaType<LinkDomain>(),
    qDBusRegisterMetaType<LinkDomainList>(),
    
    qRegisterMetaType<DnsEntry>("DnsEntry"),
    qRegisterMetaType<DnsEntryList>("DnsEntryList"),
    qRegisterMetaType<DomainEntry>("DomainEntry"),
    qRegisterMetaType<DomainEntryList>("DomainEntryList"),
    qRegisterMetaType<ResolverTransactionStatistics>("ResolverTransactionStatistics"),
    qRegisterMetaType<ResolverTransactionStatisticsList>("ResolverTransactionStatisticsList"),
    qRegisterMetaType<ResolverCacheStatistics>("ResolverCacheStatistics"),
    qRegisterMetaType<ResolverCacheStatisticsList>("ResolverCacheStatisticsList"),
    qRegisterMetaType<ResolverDNSSECStatistics>("ResolverDNSSECStatistics"),
    qRegisterMetaType<ResolverDNSSECStatisticsList>("ResolverDNSSECStatisticsList"),
    qRegisterMetaType<HostnameAddress>("HostnameAddress"),
    qRegisterMetaType<HostnameAddressList>("HostnameAddressList"),
    qRegisterMetaType<ResolveAddressName>("ResolveAddressName"),
    qRegisterMetaType<ResolveAddressNameList>("ResolveAddressNameList"),
    qRegisterMetaType<ResolveRecord>("ResolveRecord"),
    qRegisterMetaType<ResolveRecordList>("ResolveRecordList"),
    qRegisterMetaType<ResolveServiceSrvData>("ResolveServiceSrvData"),
    qRegisterMetaType<ResolveServiceSrvDataList>("ResolveServiceSrvDataList"),
    qRegisterMetaType<LinkDns>("LinkDns"),
    qRegisterMetaType<LinkDnsList>("LinkDnsList"),
    qRegisterMetaType<LinkDomain>("LinkDomain"),
    qRegisterMetaType<LinkDomainList>("LinkDomainList"),
    
};


QDBusArgument &operator<<(QDBusArgument &argument, const DnsEntry &message)
{
    argument.beginStructure();
    networkd_types::struct2dbus(&argument,
                message.index,
                message.family,
                message.address);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, DnsEntry &message)
{
    argument.beginStructure();
    networkd_types::dbus2struct(&argument,
                message.index,
                message.family,
                message.address);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const DomainEntry &message)
{
    argument.beginStructure();
    networkd_types::struct2dbus(&argument,
                message.networkInterface,
                message.name,
                message.routingOnly);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, DomainEntry &message)
{
    argument.beginStructure();
    networkd_types::dbus2struct(&argument,
                message.networkInterface,
                message.name,
                message.routingOnly);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const ResolverTransactionStatistics &message)
{
    argument.beginStructure();
    networkd_types::struct2dbus(&argument,
                message.entries,
                message.hits);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, ResolverTransactionStatistics &message)
{
    argument.beginStructure();
    networkd_types::dbus2struct(&argument,
                message.entries,
                message.hits);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const ResolverCacheStatistics &message)
{
    argument.beginStructure();
    networkd_types::struct2dbus(&argument,
                message.arg0,
                message.arg1,
                message.arg2);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, ResolverCacheStatistics &message)
{
    argument.beginStructure();
    networkd_types::dbus2struct(&argument,
                message.arg0,
                message.arg1,
                message.arg2);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const ResolverDNSSECStatistics &message)
{
    argument.beginStructure();
    networkd_types::struct2dbus(&argument,
                message.arg0,
                message.arg1,
                message.arg2,
                message.arg3);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, ResolverDNSSECStatistics &message)
{
    argument.beginStructure();
    networkd_types::dbus2struct(&argument,
                message.arg0,
                message.arg1,
                message.arg2,
                message.arg3);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const HostnameAddress &message)
{
    argument.beginStructure();
    networkd_types::struct2dbus(&argument,
                message.arg0,
                message.arg1,
                message.address);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, HostnameAddress &message)
{
    argument.beginStructure();
    networkd_types::dbus2struct(&argument,
                message.arg0,
                message.arg1,
                message.address);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const ResolveAddressName &message)
{
    argument.beginStructure();
    networkd_types::struct2dbus(&argument,
                message.index,
                message.name);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, ResolveAddressName &message)
{
    argument.beginStructure();
    networkd_types::dbus2struct(&argument,
                message.index,
                message.name);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const ResolveRecord &message)
{
    argument.beginStructure();
    networkd_types::struct2dbus(&argument,
                message.index,
                message.recordType,
                message.recordClass,
                message.rawRecordData);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, ResolveRecord &message)
{
    argument.beginStructure();
    networkd_types::dbus2struct(&argument,
                message.index,
                message.recordType,
                message.recordClass,
                message.rawRecordData);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const ResolveServiceSrvData &message)
{
    argument.beginStructure();
    networkd_types::struct2dbus(&argument,
                message.priority,
                message.weight,
                message.port,
                message.hostname,
                message.addresses,
                message.canonicalizedHostname);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, ResolveServiceSrvData &message)
{
    argument.beginStructure();
    networkd_types::dbus2struct(&argument,
                message.priority,
                message.weight,
                message.port,
                message.hostname,
                message.addresses,
                message.canonicalizedHostname);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const LinkDns &message)
{
    argument.beginStructure();
    networkd_types::struct2dbus(&argument,
                message.arg0,
                message.address);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, LinkDns &message)
{
    argument.beginStructure();
    networkd_types::dbus2struct(&argument,
                message.arg0,
                message.address);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const LinkDomain &message)
{
    argument.beginStructure();
    networkd_types::struct2dbus(&argument,
                message.domain,
                message.arg1);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, LinkDomain &message)
{
    argument.beginStructure();
    networkd_types::dbus2struct(&argument,
                message.domain,
                message.arg1);
    argument.endStructure();
    return argument;
}

