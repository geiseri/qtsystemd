
#ifndef NETWORKD_TYPES_DBUS_TYPES_H
#define NETWORKD_TYPES_DBUS_TYPES_H

#include <QString>
#include <QVariant>
#include <QDBusArgument>

struct DnsEntry {
    int index;
    int family;
    QByteArray address;
};

typedef QList<DnsEntry> DnsEntryList;

QDBusArgument &operator<<(QDBusArgument &argument, const DnsEntry &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, DnsEntry &message);

Q_DECLARE_METATYPE(DnsEntry)

struct DomainEntry {
    int networkInterface;
    QString name;
    bool routingOnly;
};

typedef QList<DomainEntry> DomainEntryList;

QDBusArgument &operator<<(QDBusArgument &argument, const DomainEntry &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, DomainEntry &message);

Q_DECLARE_METATYPE(DomainEntry)

struct ResolverTransactionStatistics {
    qulonglong entries;
    qulonglong hits;
};

typedef QList<ResolverTransactionStatistics> ResolverTransactionStatisticsList;

QDBusArgument &operator<<(QDBusArgument &argument, const ResolverTransactionStatistics &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, ResolverTransactionStatistics &message);

Q_DECLARE_METATYPE(ResolverTransactionStatistics)

struct ResolverCacheStatistics {
    qulonglong arg0;
    qulonglong arg1;
    qulonglong arg2;
};

typedef QList<ResolverCacheStatistics> ResolverCacheStatisticsList;

QDBusArgument &operator<<(QDBusArgument &argument, const ResolverCacheStatistics &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, ResolverCacheStatistics &message);

Q_DECLARE_METATYPE(ResolverCacheStatistics)

struct ResolverDNSSECStatistics {
    qulonglong arg0;
    qulonglong arg1;
    qulonglong arg2;
    qulonglong arg3;
};

typedef QList<ResolverDNSSECStatistics> ResolverDNSSECStatisticsList;

QDBusArgument &operator<<(QDBusArgument &argument, const ResolverDNSSECStatistics &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, ResolverDNSSECStatistics &message);

Q_DECLARE_METATYPE(ResolverDNSSECStatistics)

struct HostnameAddress {
    int arg0;
    int arg1;
    QByteArray address;
};

typedef QList<HostnameAddress> HostnameAddressList;

QDBusArgument &operator<<(QDBusArgument &argument, const HostnameAddress &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, HostnameAddress &message);

Q_DECLARE_METATYPE(HostnameAddress)

struct ResolveAddressName {
    int index;
    QString name;
};

typedef QList<ResolveAddressName> ResolveAddressNameList;

QDBusArgument &operator<<(QDBusArgument &argument, const ResolveAddressName &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, ResolveAddressName &message);

Q_DECLARE_METATYPE(ResolveAddressName)

struct ResolveRecord {
    int index;
    ushort recordType;
    ushort recordClass;
    QByteArray rawRecordData;
};

typedef QList<ResolveRecord> ResolveRecordList;

QDBusArgument &operator<<(QDBusArgument &argument, const ResolveRecord &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, ResolveRecord &message);

Q_DECLARE_METATYPE(ResolveRecord)

struct ResolveServiceSrvData {
    ushort priority;
    ushort weight;
    ushort port;
    QString hostname;
    HostnameAddressList addresses;
    QString canonicalizedHostname;
};

typedef QList<ResolveServiceSrvData> ResolveServiceSrvDataList;

QDBusArgument &operator<<(QDBusArgument &argument, const ResolveServiceSrvData &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, ResolveServiceSrvData &message);

Q_DECLARE_METATYPE(ResolveServiceSrvData)

struct LinkDns {
    int arg0;
    QByteArray address;
};

typedef QList<LinkDns> LinkDnsList;

QDBusArgument &operator<<(QDBusArgument &argument, const LinkDns &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, LinkDns &message);

Q_DECLARE_METATYPE(LinkDns)

struct LinkDomain {
    QString domain;
    bool arg1;
};

typedef QList<LinkDomain> LinkDomainList;

QDBusArgument &operator<<(QDBusArgument &argument, const LinkDomain &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, LinkDomain &message);

Q_DECLARE_METATYPE(LinkDomain)




#endif // NETWORKD_TYPES_DBUS_TYPES_H

