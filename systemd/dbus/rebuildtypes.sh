#!/bin/bash -e

VENV=$(mktemp -d -p $PWD)
trap "{ rm -rf ${VENV} ; exit 255; }" EXIT

virtualenv3 --system-site-packages ${VENV}

. ${VENV}/bin/activate

pip install simplejson
pip install pystache

for D in *_interfaces
do
   pushd ${D}
      for F in *_types.json
      do
          python ../generate_types.py ${F}
      done
   popd
done

exit 0
