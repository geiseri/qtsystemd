
#include "systemd_types.h"
#include <QDBusVariant>
#include <QDBusArgument>
#include <QDBusMetaType>

namespace systemd_types {

    template <typename K, typename V>
    void map2dbus(QDBusArgument *argument, const QMap<K,V> &message)
    {
        argument->beginMap( qMetaTypeId<K>(), qMetaTypeId<V>()  );
        foreach( K key, message.keys() ) {
            argument->beginMapEntry();
            (*argument) << key << message[key];
            argument->endMapEntry();
        }
        argument->endMap();
    }


    template <typename K, typename V>
    void dbus2map(const QDBusArgument *argument, QMap<K,V> &message)
    {
        argument->beginMap();
        message.clear();
        while ( !argument->atEnd() ) {
            K key;
            V value;
            argument->beginMapEntry();
            (*argument) >> key >> value;
            argument->endMapEntry();
            message[key] = value;
        }
        argument->endMap();
    }


    // This is our base-case for the print function:
    void struct2dbus(QDBusArgument *argument, const QVariant& msg)
    {
      (*argument) << QDBusVariant(msg);
    }

    template <class T>
    void struct2dbus(QDBusArgument *argument, const T& msg)
    {
      (*argument) << msg;
    }

    template <class T>
    void dbus2struct( const QDBusArgument *argument, T& msg)
    {
      (*argument) >> msg;
    }

    // And this is the recursive case:
    template <class A, class... B>
    void struct2dbus( QDBusArgument *argument, const A &head, B... tail)
    {
        struct2dbus(argument, head);
        struct2dbus(argument, tail...);
    }

    template <class A, class... B>
    void dbus2struct( const QDBusArgument *argument, A& head, B&... tail)
    {
        dbus2struct(argument, head);
        dbus2struct(argument, tail...);
    }
}

static int s_systemd_types_typeids[] = {

    qDBusRegisterMetaType<UnitProperty>(),
    qDBusRegisterMetaType<UnitPropertyList>(),
    qDBusRegisterMetaType<SystemdAux>(),
    qDBusRegisterMetaType<SystemdAuxList>(),
    qDBusRegisterMetaType<UnitChange>(),
    qDBusRegisterMetaType<UnitChangeList>(),
    qDBusRegisterMetaType<SystemdUnitCondition>(),
    qDBusRegisterMetaType<SystemdUnitConditionList>(),
    qDBusRegisterMetaType<SystemdJob>(),
    qDBusRegisterMetaType<SystemdJobList>(),
    qDBusRegisterMetaType<SystemdUnit>(),
    qDBusRegisterMetaType<SystemdUnitList>(),
    qDBusRegisterMetaType<SELinuxContextType>(),
    qDBusRegisterMetaType<SELinuxContextTypeList>(),
    qDBusRegisterMetaType<SystemCallFilterType>(),
    qDBusRegisterMetaType<SystemCallFilterTypeList>(),
    qDBusRegisterMetaType<MountExecLine>(),
    qDBusRegisterMetaType<MountExecLineList>(),
    qDBusRegisterMetaType<ServiceExecLine>(),
    qDBusRegisterMetaType<ServiceExecLineList>(),
    qDBusRegisterMetaType<SocketExecLine>(),
    qDBusRegisterMetaType<SocketExecLineList>(),
    qDBusRegisterMetaType<CalendarTimer>(),
    qDBusRegisterMetaType<CalendarTimerList>(),
    qDBusRegisterMetaType<MonotonicTimer>(),
    qDBusRegisterMetaType<MonotonicTimerList>(),
    qDBusRegisterMetaType<JobReference>(),
    qDBusRegisterMetaType<JobReferenceList>(),
    qDBusRegisterMetaType<UnitError>(),
    qDBusRegisterMetaType<UnitErrorList>(),
    
    qRegisterMetaType<UnitProperty>("UnitProperty"),
    qRegisterMetaType<UnitPropertyList>("UnitPropertyList"),
    qRegisterMetaType<SystemdAux>("SystemdAux"),
    qRegisterMetaType<SystemdAuxList>("SystemdAuxList"),
    qRegisterMetaType<UnitChange>("UnitChange"),
    qRegisterMetaType<UnitChangeList>("UnitChangeList"),
    qRegisterMetaType<SystemdUnitCondition>("SystemdUnitCondition"),
    qRegisterMetaType<SystemdUnitConditionList>("SystemdUnitConditionList"),
    qRegisterMetaType<SystemdJob>("SystemdJob"),
    qRegisterMetaType<SystemdJobList>("SystemdJobList"),
    qRegisterMetaType<SystemdUnit>("SystemdUnit"),
    qRegisterMetaType<SystemdUnitList>("SystemdUnitList"),
    qRegisterMetaType<SELinuxContextType>("SELinuxContextType"),
    qRegisterMetaType<SELinuxContextTypeList>("SELinuxContextTypeList"),
    qRegisterMetaType<SystemCallFilterType>("SystemCallFilterType"),
    qRegisterMetaType<SystemCallFilterTypeList>("SystemCallFilterTypeList"),
    qRegisterMetaType<MountExecLine>("MountExecLine"),
    qRegisterMetaType<MountExecLineList>("MountExecLineList"),
    qRegisterMetaType<ServiceExecLine>("ServiceExecLine"),
    qRegisterMetaType<ServiceExecLineList>("ServiceExecLineList"),
    qRegisterMetaType<SocketExecLine>("SocketExecLine"),
    qRegisterMetaType<SocketExecLineList>("SocketExecLineList"),
    qRegisterMetaType<CalendarTimer>("CalendarTimer"),
    qRegisterMetaType<CalendarTimerList>("CalendarTimerList"),
    qRegisterMetaType<MonotonicTimer>("MonotonicTimer"),
    qRegisterMetaType<MonotonicTimerList>("MonotonicTimerList"),
    qRegisterMetaType<JobReference>("JobReference"),
    qRegisterMetaType<JobReferenceList>("JobReferenceList"),
    qRegisterMetaType<UnitError>("UnitError"),
    qRegisterMetaType<UnitErrorList>("UnitErrorList"),
    
};


QDBusArgument &operator<<(QDBusArgument &argument, const UnitProperty &message)
{
    argument.beginStructure();
    systemd_types::struct2dbus(&argument,
                message.key,
                message.value);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, UnitProperty &message)
{
    argument.beginStructure();
    systemd_types::dbus2struct(&argument,
                message.key,
                message.value);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const SystemdAux &message)
{
    argument.beginStructure();
    systemd_types::struct2dbus(&argument,
                message.name,
                message.properties);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, SystemdAux &message)
{
    argument.beginStructure();
    systemd_types::dbus2struct(&argument,
                message.name,
                message.properties);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const UnitChange &message)
{
    argument.beginStructure();
    systemd_types::struct2dbus(&argument,
                message.type,
                message.path,
                message.source);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, UnitChange &message)
{
    argument.beginStructure();
    systemd_types::dbus2struct(&argument,
                message.type,
                message.path,
                message.source);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const SystemdUnitCondition &message)
{
    argument.beginStructure();
    systemd_types::struct2dbus(&argument,
                message.name,
                message.trigger,
                message.negate,
                message.param,
                message.state);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, SystemdUnitCondition &message)
{
    argument.beginStructure();
    systemd_types::dbus2struct(&argument,
                message.name,
                message.trigger,
                message.negate,
                message.param,
                message.state);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const SystemdJob &message)
{
    argument.beginStructure();
    systemd_types::struct2dbus(&argument,
                message.id,
                message.unitId,
                message.type,
                message.state,
                message.path,
                message.unitPath);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, SystemdJob &message)
{
    argument.beginStructure();
    systemd_types::dbus2struct(&argument,
                message.id,
                message.unitId,
                message.type,
                message.state,
                message.path,
                message.unitPath);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const SystemdUnit &message)
{
    argument.beginStructure();
    systemd_types::struct2dbus(&argument,
                message.id,
                message.description,
                message.loadState,
                message.activeState,
                message.subState,
                message.following,
                message.path,
                message.jobId,
                message.jobType,
                message.jobPath);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, SystemdUnit &message)
{
    argument.beginStructure();
    systemd_types::dbus2struct(&argument,
                message.id,
                message.description,
                message.loadState,
                message.activeState,
                message.subState,
                message.following,
                message.path,
                message.jobId,
                message.jobType,
                message.jobPath);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const SELinuxContextType &message)
{
    argument.beginStructure();
    systemd_types::struct2dbus(&argument,
                message.a1,
                message.a2);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, SELinuxContextType &message)
{
    argument.beginStructure();
    systemd_types::dbus2struct(&argument,
                message.a1,
                message.a2);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const SystemCallFilterType &message)
{
    argument.beginStructure();
    systemd_types::struct2dbus(&argument,
                message.a1,
                message.a2);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, SystemCallFilterType &message)
{
    argument.beginStructure();
    systemd_types::dbus2struct(&argument,
                message.a1,
                message.a2);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const MountExecLine &message)
{
    argument.beginStructure();
    systemd_types::struct2dbus(&argument,
                message.exec,
                message.args,
                message.a3,
                message.a4,
                message.a5,
                message.a6,
                message.a7,
                message.a8,
                message.a9,
                message.a10);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, MountExecLine &message)
{
    argument.beginStructure();
    systemd_types::dbus2struct(&argument,
                message.exec,
                message.args,
                message.a3,
                message.a4,
                message.a5,
                message.a6,
                message.a7,
                message.a8,
                message.a9,
                message.a10);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const ServiceExecLine &message)
{
    argument.beginStructure();
    systemd_types::struct2dbus(&argument,
                message.exec,
                message.args,
                message.a3,
                message.a4,
                message.a5,
                message.a6,
                message.a7,
                message.a8,
                message.a9,
                message.a10);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, ServiceExecLine &message)
{
    argument.beginStructure();
    systemd_types::dbus2struct(&argument,
                message.exec,
                message.args,
                message.a3,
                message.a4,
                message.a5,
                message.a6,
                message.a7,
                message.a8,
                message.a9,
                message.a10);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const SocketExecLine &message)
{
    argument.beginStructure();
    systemd_types::struct2dbus(&argument,
                message.exec,
                message.args,
                message.a3,
                message.a4,
                message.a5,
                message.a6,
                message.a7,
                message.a8,
                message.a9,
                message.a10);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, SocketExecLine &message)
{
    argument.beginStructure();
    systemd_types::dbus2struct(&argument,
                message.exec,
                message.args,
                message.a3,
                message.a4,
                message.a5,
                message.a6,
                message.a7,
                message.a8,
                message.a9,
                message.a10);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const CalendarTimer &message)
{
    argument.beginStructure();
    systemd_types::struct2dbus(&argument,
                message.a1,
                message.a2,
                message.a3);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, CalendarTimer &message)
{
    argument.beginStructure();
    systemd_types::dbus2struct(&argument,
                message.a1,
                message.a2,
                message.a3);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const MonotonicTimer &message)
{
    argument.beginStructure();
    systemd_types::struct2dbus(&argument,
                message.a1,
                message.a2,
                message.a3);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, MonotonicTimer &message)
{
    argument.beginStructure();
    systemd_types::dbus2struct(&argument,
                message.a1,
                message.a2,
                message.a3);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const JobReference &message)
{
    argument.beginStructure();
    systemd_types::struct2dbus(&argument,
                message.id,
                message.path);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, JobReference &message)
{
    argument.beginStructure();
    systemd_types::dbus2struct(&argument,
                message.id,
                message.path);
    argument.endStructure();
    return argument;
}
QDBusArgument &operator<<(QDBusArgument &argument, const UnitError &message)
{
    argument.beginStructure();
    systemd_types::struct2dbus(&argument,
                message.error,
                message.error_text);
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, UnitError &message)
{
    argument.beginStructure();
    systemd_types::dbus2struct(&argument,
                message.error,
                message.error_text);
    argument.endStructure();
    return argument;
}

