
#ifndef SYSTEMD_TYPES_DBUS_TYPES_H
#define SYSTEMD_TYPES_DBUS_TYPES_H

#include <QString>
#include <QVariant>
#include <QDBusArgument>

struct UnitProperty {
    QString key;
    QVariant value;
};

typedef QList<UnitProperty> UnitPropertyList;

QDBusArgument &operator<<(QDBusArgument &argument, const UnitProperty &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, UnitProperty &message);

Q_DECLARE_METATYPE(UnitProperty)

struct SystemdAux {
    QString name;
    UnitPropertyList properties;
};

typedef QList<SystemdAux> SystemdAuxList;

QDBusArgument &operator<<(QDBusArgument &argument, const SystemdAux &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, SystemdAux &message);

Q_DECLARE_METATYPE(SystemdAux)

struct UnitChange {
    QString type;
    QString path;
    QString source;
};

typedef QList<UnitChange> UnitChangeList;

QDBusArgument &operator<<(QDBusArgument &argument, const UnitChange &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, UnitChange &message);

Q_DECLARE_METATYPE(UnitChange)

struct SystemdUnitCondition {
    QString name;
    bool trigger;
    bool negate;
    QString param;
    qint32 state;
};

typedef QList<SystemdUnitCondition> SystemdUnitConditionList;

QDBusArgument &operator<<(QDBusArgument &argument, const SystemdUnitCondition &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, SystemdUnitCondition &message);

Q_DECLARE_METATYPE(SystemdUnitCondition)

struct SystemdJob {
    quint32 id;
    QString unitId;
    QString type;
    QString state;
    QDBusObjectPath path;
    QDBusObjectPath unitPath;
};

typedef QList<SystemdJob> SystemdJobList;

QDBusArgument &operator<<(QDBusArgument &argument, const SystemdJob &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, SystemdJob &message);

Q_DECLARE_METATYPE(SystemdJob)

struct SystemdUnit {
    QString id;
    QString description;
    QString loadState;
    QString activeState;
    QString subState;
    QString following;
    QDBusObjectPath path;
    quint32 jobId;
    QString jobType;
    QDBusObjectPath jobPath;
};

typedef QList<SystemdUnit> SystemdUnitList;

QDBusArgument &operator<<(QDBusArgument &argument, const SystemdUnit &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, SystemdUnit &message);

Q_DECLARE_METATYPE(SystemdUnit)

struct SELinuxContextType {
    bool a1;
    QString a2;
};

typedef QList<SELinuxContextType> SELinuxContextTypeList;

QDBusArgument &operator<<(QDBusArgument &argument, const SELinuxContextType &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, SELinuxContextType &message);

Q_DECLARE_METATYPE(SELinuxContextType)

struct SystemCallFilterType {
    bool a1;
    QString a2;
};

typedef QList<SystemCallFilterType> SystemCallFilterTypeList;

QDBusArgument &operator<<(QDBusArgument &argument, const SystemCallFilterType &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, SystemCallFilterType &message);

Q_DECLARE_METATYPE(SystemCallFilterType)

struct MountExecLine {
    QString exec;
    QStringList args;
    bool a3;
    qulonglong a4;
    qulonglong a5;
    qulonglong a6;
    qulonglong a7;
    quint32 a8;
    qint32 a9;
    qint32 a10;
};

typedef QList<MountExecLine> MountExecLineList;

QDBusArgument &operator<<(QDBusArgument &argument, const MountExecLine &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, MountExecLine &message);

Q_DECLARE_METATYPE(MountExecLine)

struct ServiceExecLine {
    QString exec;
    QStringList args;
    bool a3;
    qulonglong a4;
    qulonglong a5;
    qulonglong a6;
    qulonglong a7;
    quint32 a8;
    qint32 a9;
    qint32 a10;
};

typedef QList<ServiceExecLine> ServiceExecLineList;

QDBusArgument &operator<<(QDBusArgument &argument, const ServiceExecLine &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, ServiceExecLine &message);

Q_DECLARE_METATYPE(ServiceExecLine)

struct SocketExecLine {
    QString exec;
    QStringList args;
    bool a3;
    qulonglong a4;
    qulonglong a5;
    qulonglong a6;
    qulonglong a7;
    quint32 a8;
    qint32 a9;
    qint32 a10;
};

typedef QList<SocketExecLine> SocketExecLineList;

QDBusArgument &operator<<(QDBusArgument &argument, const SocketExecLine &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, SocketExecLine &message);

Q_DECLARE_METATYPE(SocketExecLine)

struct CalendarTimer {
    QString a1;
    QString a2;
    qulonglong a3;
};

typedef QList<CalendarTimer> CalendarTimerList;

QDBusArgument &operator<<(QDBusArgument &argument, const CalendarTimer &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, CalendarTimer &message);

Q_DECLARE_METATYPE(CalendarTimer)

struct MonotonicTimer {
    QString a1;
    qulonglong a2;
    qulonglong a3;
};

typedef QList<MonotonicTimer> MonotonicTimerList;

QDBusArgument &operator<<(QDBusArgument &argument, const MonotonicTimer &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, MonotonicTimer &message);

Q_DECLARE_METATYPE(MonotonicTimer)

struct JobReference {
    int id;
    QDBusObjectPath path;
};

typedef QList<JobReference> JobReferenceList;

QDBusArgument &operator<<(QDBusArgument &argument, const JobReference &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, JobReference &message);

Q_DECLARE_METATYPE(JobReference)

struct UnitError {
    QString error;
    QString error_text;
};

typedef QList<UnitError> UnitErrorList;

QDBusArgument &operator<<(QDBusArgument &argument, const UnitError &message);
const QDBusArgument &operator>>(const QDBusArgument &argument, UnitError &message);

Q_DECLARE_METATYPE(UnitError)




#endif // SYSTEMD_TYPES_DBUS_TYPES_H

