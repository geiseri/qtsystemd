 # qtsystemd - A Qt5 wrapper to the systemd/network/timedated/machine API.
 # Copyright (C) 2017 Ian Reinhart Geiser
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU Lesser General Public License as
 # published by the Free Software Foundation, either version 2.1 of the
 # License, or (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 # GNU Lesser General Public License for more details.
 #
 # You should have received a copy of the GNU Lesser General Public License
 # along with this program. If not, see <http://www.gnu.org/licenses/>.

TEMPLATE = lib
TARGET = timedated_interfaces
CONFIG += dbus static  c++11
INCLUDEPATH += $$TOP_SRCDIR/systemd/dbus \
               $$TOP_OUTDIR/systemd/dbus
QT += dbus
QT -= gui

HEADERS += timedated_types.h
SOURCES += timedated_types.cpp

QDBUSXML2CPP_INTERFACE_HEADER_FLAGS = -i timedated_interfaces/timedated_types.h -i dbus_interfaces/dbus_types.h
QDBUSXML2CPP_ADAPTOR_HEADER_FLAGS = -i timedated_interfaces/timedated_types.h -i dbus_interfaces/dbus_types.h

DBUS_INTERFACES += \
    org.freedesktop.timedate1.xml

DISTFILES += \
    timedated_types.json




