/*
 * qtsystemd - A Qt5 wrapper to the systemd/logind/machine API.
 * Copyright (C) 2015 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DBUSMIXIN_H
#define DBUSMIXIN_H

#include <typeinfo>
#include <QString>
#include <QDBusConnection>
#include <QDBusAbstractInterface>


class BaseDBusMixin
{
public:
     BaseDBusMixin( const QString &service, const QString &path, const QDBusConnection &connection);
     virtual ~BaseDBusMixin();

     QString service() const;
     QString path() const;
     QDBusConnection connection() const;

     template<typename T> struct PolyHelper { };
     template <typename X> X *interface( BaseDBusMixin::PolyHelper<X>) const;

private:
     QString m_service;
     QString m_path;
     QDBusConnection m_connection;
};

template< typename B , typename T = BaseDBusMixin>
class DBusMixin : public T
{
public:
    DBusMixin( const QString &service, const QString &path, const QDBusConnection &connection) :
        T(service,path,connection), m_dbusInterface(new B(service,path,connection))
    {
    }

    ~DBusMixin()
    {
        delete m_dbusInterface;
    }

    using T::interface;
    B* interface( BaseDBusMixin::PolyHelper<B> ) const
    {
        return m_dbusInterface;
    }

    QDBusError lastError() const
    {
        return m_dbusInterface->lastError();
    }

private:
    B *m_dbusInterface;
};


template<class T>
class DBusMixinAdaptor
{
public:
    DBusMixinAdaptor( const QString &service, const QString &path, const QDBusConnection &connection)
    {
        m_adaptor = new T(service,path,connection);
    }

    ~DBusMixinAdaptor()
    {
        delete m_adaptor;
    }

    template<typename X>
    X* interface() const
    {
        return m_adaptor->interface(BaseDBusMixin::PolyHelper<X>());
    }

    QString service() const
    {
        return m_adaptor->service();
    }

    QDBusConnection connection() const
    {
        return m_adaptor->connection();
    }

    QString	path() const
    {
        return m_adaptor->path();
    }

private:
    T *m_adaptor;

};
#endif // DBUSMIXIN_H
