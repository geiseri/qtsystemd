/*
 * qtsystemd - A Qt5 wrapper to the systemd/logind/machine API.
 * Copyright (C) 2015 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DBUSPROPERTIES_H
#define DBUSPROPERTIES_H

#include <QDBusConnection>
#include <dbus_interfaces/dbus_types.h>
#include <dbus_interfaces/properties_interface.h>
#include <dbusmixin.h>

typedef DBusMixinAdaptor< DBusMixin <org::freedesktop::DBus::Properties> > DBusPropertiesAdaptor;

namespace DBus {
    class Properties : public DBusPropertiesAdaptor
    {
    public:
        explicit Properties(const QString &interface, const QString &path, const QDBusConnection &conn = QDBusConnection::sessionBus());
        ~Properties();
    };
}


#endif // DBUSPROPERTIES_H

