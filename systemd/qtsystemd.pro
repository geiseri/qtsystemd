 # qtsystemd - A Qt5 wrapper to the systemd/logind/machine API.
 # Copyright (C) 2015 Ian Reinhart Geiser
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU Lesser General Public License as
 # published by the Free Software Foundation, either version 2.1 of the
 # License, or (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 # GNU Lesser General Public License for more details.
 #
 # You should have received a copy of the GNU Lesser General Public License
 # along with this program. If not, see <http://www.gnu.org/licenses/>.

TEMPLATE = lib
TARGET = qtsystemd
TARGET = $$qtLibraryTarget($$TARGET)

QT += dbus
QT -= gui

CONFIG += c++11
# systemd version xml was taken from
VER_MAJ = 234
# API changes to the qtsystemd interface
VER_MIN = 1
# Bugfixes
VER_PAT = 0

INCLUDEPATH += . \
               $$TOP_SRCDIR/systemd/adaptors \
               $$TOP_SRCDIR/systemd/dbus \
               $$TOP_OUTDIR/systemd/adaptors \
               $$TOP_OUTDIR/systemd/dbus \
               $$TOP_SRCDIR/systemd
# Input
HEADERS += \
           statetracker.h \
           dbusproperties.h \
    systemdbus.h \
    dbusmixin.h \
    scoperunner.h \
    scopeprocesscontroller.h \
    kpty_p.h \
    kpty.h

SOURCES += \
           statetracker.cpp \
    dbusproperties.cpp \
    systemdbus.cpp \
    dbusmixin.cpp \
    scoperunner.cpp \
    scopeprocesscontroller.cpp \
    kpty.cpp

DEFINES += HAVE_POSIX_OPENPT \
           HAVE_PTY_H \
           HAVE_TERMIO_H \
           HAVE_SYS_STROPTS_H \
           HAVE_INITGROUPS


LIBS += -Wl,--whole-archive -L$$OUT_PWD/dbus/dbus_interfaces -ldbus_interfaces -Wl,--no-whole-archive
DEPENDPATH += $${PWD}/dbus/dbus
PRE_TARGETDEPS += $${OUT_PWD}/dbus/dbus_interfaces/libdbus_interfaces.a

LIBS += -Wl,--whole-archive -L$${OUT_PWD}/dbus/systemd_interfaces -lsystemd_interfaces -Wl,--no-whole-archive
DEPENDPATH += $${PWD}/dbus/systemd
PRE_TARGETDEPS += $${OUT_PWD}/dbus/systemd_interfaces/libsystemd_interfaces.a

LIBS += -Wl,--whole-archive -L$${OUT_PWD}/dbus/machined_interfaces -lmachined_interfaces -Wl,--no-whole-archive
DEPENDPATH += $${PWD}/dbus/machined
PRE_TARGETDEPS += $${OUT_PWD}/dbus/machined_interfaces/libmachined_interfaces.a

LIBS += -Wl,--whole-archive -L$${OUT_PWD}/dbus/logind_interfaces -llogind_interfaces -Wl,--no-whole-archive
DEPENDPATH += $${PWD}/dbus/logind
PRE_TARGETDEPS += $${OUT_PWD}/dbus/logind_interfaces/liblogind_interfaces.a

LIBS += -Wl,--whole-archive -L$${OUT_PWD}/dbus/networkd_interfaces -lnetworkd_interfaces -Wl,--no-whole-archive
DEPENDPATH += $${PWD}/dbus/networkd
PRE_TARGETDEPS += $${OUT_PWD}/dbus/networkd_interfaces/libnetworkd_interfaces.a

LIBS += -Wl,--whole-archive -L$${OUT_PWD}/dbus/localed_interfaces -llocaled_interfaces -Wl,--no-whole-archive
DEPENDPATH += $${PWD}/dbus/localed
PRE_TARGETDEPS += $${OUT_PWD}/dbus/localed_interfaces/liblocaled_interfaces.a

LIBS += -Wl,--whole-archive -L$${OUT_PWD}/dbus/timedated_interfaces -ltimedated_interfaces -Wl,--no-whole-archive
DEPENDPATH += $${PWD}/dbus/timedated
PRE_TARGETDEPS += $${OUT_PWD}/dbus/timedated_interfaces/libtimedated_interfaces.a

LIBS += -Wl,--whole-archive -L$${OUT_PWD}/dbus/hostnamed_interfaces -lhostnamed_interfaces -Wl,--no-whole-archive
DEPENDPATH += $${PWD}/dbus/hostnamed
PRE_TARGETDEPS += $${OUT_PWD}/dbus/hostnamed_interfaces/libhostnamed_interfaces.a


LIBS += -Wl,--whole-archive -L$${OUT_PWD}/adaptors/systemd -lsystemd_adaptors -Wl,--no-whole-archive
DEPENDPATH += $${PWD}/adaptors/systemd
PRE_TARGETDEPS += $${OUT_PWD}/adaptors/systemd/libsystemd_adaptors.a

LIBS += -Wl,--whole-archive -L$${OUT_PWD}/adaptors/machined -lmachined_adaptors -Wl,--no-whole-archive
DEPENDPATH += $${PWD}/adaptors/machined
PRE_TARGETDEPS += $${OUT_PWD}/adaptors/machined/libmachined_adaptors.a

LIBS += -Wl,--whole-archive -L$${OUT_PWD}/adaptors/logind -llogind_adaptors -Wl,--no-whole-archive
DEPENDPATH += $${PWD}/adaptors/logind
PRE_TARGETDEPS += $${OUT_PWD}/adaptors/logind/liblogind_adaptors.a

LIBS += -Wl,--whole-archive -L$${OUT_PWD}/adaptors/networkd -lnetworkd_adaptors -Wl,--no-whole-archive
DEPENDPATH += $${PWD}/adaptors/networkd
PRE_TARGETDEPS += $${OUT_PWD}/adaptors/networkd/libnetworkd_adaptors.a

LIBS += -Wl,--whole-archive -L$${OUT_PWD}/adaptors/localed -llocaled_adaptors -Wl,--no-whole-archive
DEPENDPATH += $${PWD}/adaptors/localed
PRE_TARGETDEPS += $${OUT_PWD}/adaptors/localed/liblocaled_adaptors.a

LIBS += -Wl,--whole-archive -L$${OUT_PWD}/adaptors/timedated -ltimedated_adaptors -Wl,--no-whole-archive
DEPENDPATH += $${PWD}/adaptors/timedated
PRE_TARGETDEPS += $${OUT_PWD}/adaptors/timedated/libtimedated_adaptors.a

LIBS += -Wl,--whole-archive -L$${OUT_PWD}/adaptors/hostnamed -lhostnamed_adaptors -Wl,--no-whole-archive
DEPENDPATH += $${PWD}/adaptors/hostnamed
PRE_TARGETDEPS += $${OUT_PWD}/adaptors/hostnamed/libhostnamed_adaptors.a

CONFIG += create_pc create_prl no_install_prl


!defined( PREFIX, var ) {
    PREFIX=/usr
}


headers.files = $${PWD}/*.h
headers.path = $${PREFIX}/include/$${TARGET}
INSTALLS += headers

systemd_headers.files = $${PWD}/adaptors/systemd/*.h
systemd_headers.path = $${PREFIX}/include/$${TARGET}/systemd
INSTALLS += systemd_headers

machined_headers.files = $${PWD}/adaptors/machined/*.h
machined_headers.path = $${PREFIX}/include/$${TARGET}/machined
INSTALLS += machined_headers

logind_headers.files = $${PWD}/adaptors/logind/*.h
logind_headers.path = $${PREFIX}/include/$${TARGET}/logind
INSTALLS += logind_headers

networkd_headers.files = $${PWD}/adaptors/networkd/*.h
networkd_headers.path = $${PREFIX}/include/$${TARGET}/networkd
INSTALLS += networkd_headers

localed_headers.files = $${PWD}/adaptors/localed/*.h
localed_headers.path = $${PREFIX}/include/$${TARGET}/localed
INSTALLS += localed_headers

timedated_headers.files = $${PWD}/adaptors/timedated/*.h
timedated_headers.path = $${PREFIX}/include/$${TARGET}/timedated
INSTALLS += timedated_headers

hostnamed_headers.files = $${PWD}/adaptors/hostnamed/*.h
hostnamed_headers.path = $${PREFIX}/include/$${TARGET}/hostnamed
INSTALLS += hostnamed_headers


dbus_interfaces.files = $${OUT_PWD}/dbus/dbus_interfaces/properties_interface.h \
                        $${PWD}/dbus/dbus_interfaces/*.h
dbus_interfaces.path = $${PREFIX}/include/$${TARGET}/dbus_interfaces
dbus_interfaces.CONFIG += no_check_exist
INSTALLS += dbus_interfaces

systemd_interfaces.files = $${OUT_PWD}/dbus/systemd_interfaces/timer_interface.h \
                           $${OUT_PWD}/dbus/systemd_interfaces/target_interface.h \
                           $${OUT_PWD}/dbus/systemd_interfaces/device_interface.h \
                           $${OUT_PWD}/dbus/systemd_interfaces/socket_interface.h \
                           $${OUT_PWD}/dbus/systemd_interfaces/slice_interface.h \
                           $${OUT_PWD}/dbus/systemd_interfaces/unit_interface.h \
                           $${OUT_PWD}/dbus/systemd_interfaces/service_interface.h \
                           $${OUT_PWD}/dbus/systemd_interfaces/mount_interface.h \
                           $${OUT_PWD}/dbus/systemd_interfaces/scope_interface.h \
                           $${OUT_PWD}/dbus/systemd_interfaces/automount_interface.h \
                           $${OUT_PWD}/dbus/systemd_interfaces/systemdmanager_interface.h \
                           $${OUT_PWD}/dbus/systemd_interfaces/path_interface.h \
                           $${PWD}/dbus/systemd_interfaces/*.h
systemd_interfaces.path = $${PREFIX}/include/$${TARGET}/systemd_interfaces
systemd_interfaces.CONFIG += no_check_exist
INSTALLS += systemd_interfaces

machined_interfaces.files = $${OUT_PWD}/dbus/machined_interfaces/machine_interface.h \
                            $${OUT_PWD}/dbus/machined_interfaces/machinemanager_interface.h \
                            $${PWD}/dbus/machined_interfaces/*.h
machined_interfaces.path = $${PREFIX}/include/$${TARGET}/machined_interfaces
machined_interfaces.CONFIG += no_check_exist
INSTALLS += machined_interfaces

logind_interfaces.files = $${OUT_PWD}/dbus/logind_interfaces/loginmanager_interface.h \
                          $${OUT_PWD}/dbus/logind_interfaces/session_interface.h \
                          $${OUT_PWD}/dbus/logind_interfaces/user_interface.h \
                          $${OUT_PWD}/dbus/logind_interfaces/seat_interface.h \
                          $${PWD}/dbus/logind_interfaces/*.h
logind_interfaces.path = $${PREFIX}/include/$${TARGET}/logind_interfaces
logind_interfaces.CONFIG += no_check_exist
INSTALLS += logind_interfaces

networkd_interfaces.files = $${OUT_PWD}/dbus/networkd_interfaces/resolve1_interface.h \
                            $${OUT_PWD}/dbus/networkd_interfaces/link_interface.h \
                            $${OUT_PWD}/dbus/networkd_interfaces/networkmanager_interface.h \
                            $${OUT_PWD}/dbus/networkd_interfaces/network_interface.h \
                            $${PWD}/dbus/networkd_interfaces/*.h
networkd_interfaces.path = $${PREFIX}/include/$${TARGET}/networkd_interfaces
networkd_interfaces.CONFIG += no_check_exist
INSTALLS += networkd_interfaces

localed_interfaces.files = $${OUT_PWD}/dbus/localed_interfaces/locale1_interface.h \
                           $${PWD}/dbus/localed_interfaces/*.h
localed_interfaces.path = $${PREFIX}/include/$${TARGET}/localed_interfaces
localed_interfaces.CONFIG += no_check_exist
INSTALLS += localed_interfaces

timedated_interfaces.files = $${OUT_PWD}/dbus/timedated_interfaces/timedate1_interface.h \
                             $${PWD}/dbus/timedated_interfaces/*.h
timedated_interfaces.path = $${PREFIX}/include/$${TARGET}/timedated_interfaces
timedated_interfaces.CONFIG += no_check_exist
INSTALLS += timedated_interfaces

hostnamed_interfaces.files = $${OUT_PWD}/dbus/hostnamed_interfaces/hostname1_interface.h \
                             $${PWD}/dbus/hostnamed_interfaces/*.h
hostnamed_interfaces.path = $${PREFIX}/include/$${TARGET}/hostnamed_interfaces
hostnamed_interfaces.CONFIG += no_check_exist
INSTALLS += hostnamed_interfaces

target.path = $${PREFIX}/lib
INSTALLS += target

QMAKE_PKGCONFIG_NAME = Qt5Systemd
QMAKE_PKGCONFIG_DESCRIPTION = Qt5 interface to systemd facilities
QMAKE_PKGCONFIG_PREFIX = $${PREFIX}
QMAKE_PKGCONFIG_LIBDIR = $${target.path}
QMAKE_PKGCONFIG_INCDIR = $${headers.path}
QMAKE_PKGCONFIG_VERSION = $${VER_MAJ}.$${VER_MIN}.$${VER_PAT}
QMAKE_PKGCONFIG_CFLAGS += -std=c++0x
QMAKE_PKGCONFIG_REQUIRES = Qt5DBus
QMAKE_PKGCONFIG_DESTDIR = pkgconfig

