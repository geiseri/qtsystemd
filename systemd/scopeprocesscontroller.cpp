#include "scopeprocesscontroller.h"


#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

#include <QtCore/QSocketNotifier>
#include <QDebug>

class ScopeProcessControllerPrivate
{
public:
    ScopeProcessControllerPrivate(ScopeProcessController *parent)
        : needcheck( false ),
          notifier( 0 ),
          q_ptr(parent)
    {
    }

    ~ScopeProcessControllerPrivate()
    {
        delete notifier;
    }

    int fd[2];
    bool needcheck;
    QSocketNotifier *notifier;
    QList<ScopeRunner*> scopeProcessList;
    QList<int> unixProcessList;
    ScopeProcessController *q_ptr;

    static struct sigaction oldChildHandlerData;
    static bool handlerSet;
    static int refCount;
    static ScopeProcessController* instance;
};

ScopeProcessController *ScopeProcessControllerPrivate::instance = 0;
int ScopeProcessControllerPrivate::refCount = 0;

void ScopeProcessController::ref()
{
    if ( !ScopeProcessControllerPrivate::refCount ) {
        ScopeProcessControllerPrivate::instance = new ScopeProcessController;
        setupHandlers();
    }
    ScopeProcessControllerPrivate::refCount++;
}

void ScopeProcessController::deref()
{
    ScopeProcessControllerPrivate::refCount--;
    if( !ScopeProcessControllerPrivate::refCount ) {
        resetHandlers();
        delete ScopeProcessControllerPrivate::instance;
        ScopeProcessControllerPrivate::instance = 0;
    }
}

ScopeProcessController* ScopeProcessController::instance()
{

    return ScopeProcessControllerPrivate::instance;
}

ScopeProcessController::ScopeProcessController()
    : d_ptr( new ScopeProcessControllerPrivate(this) )
{
    Q_D(ScopeProcessController);
    if( pipe( d->fd ) )
    {
        perror( "pipe" );
        abort();
    }

    fcntl( d->fd[0], F_SETFL, O_NONBLOCK ); // in case slotDoHousekeeping is called without polling first
    fcntl( d->fd[1], F_SETFL, O_NONBLOCK ); // in case it fills up
    fcntl( d->fd[0], F_SETFD, FD_CLOEXEC );
    fcntl( d->fd[1], F_SETFD, FD_CLOEXEC );

    d->notifier = new QSocketNotifier( d->fd[0], QSocketNotifier::Read );
    d->notifier->setEnabled( true );
    QObject::connect( d->notifier, SIGNAL(activated(int)),
                      SLOT(slotDoHousekeeping()));
}

ScopeProcessController::~ScopeProcessController()
{
    Q_D(ScopeProcessController);

    close( d->fd[0] );
    close( d->fd[1] );
}


extern "C" {
    static void theReaper( int num )
    {
        ScopeProcessController::theSigCHLDHandler( num );
    }
}

struct sigaction ScopeProcessControllerPrivate::oldChildHandlerData;

bool ScopeProcessControllerPrivate::handlerSet = false;

void ScopeProcessController::setupHandlers()
{
    if( ScopeProcessControllerPrivate::handlerSet )
        return;
    ScopeProcessControllerPrivate::handlerSet = true;

    struct sigaction act;
    sigemptyset( &act.sa_mask );

    act.sa_handler = SIG_IGN;
    act.sa_flags = 0;
    sigaction( SIGPIPE, &act, 0L );

    act.sa_handler = theReaper;
    act.sa_flags = SA_NOCLDSTOP;
    // CC: take care of SunOS which automatically restarts interrupted system
    // calls (and thus does not have SA_RESTART)
#ifdef SA_RESTART
    act.sa_flags |= SA_RESTART;
#endif
    sigaction( SIGCHLD, &act, &ScopeProcessControllerPrivate::oldChildHandlerData );

    sigaddset( &act.sa_mask, SIGCHLD );
    // Make sure we don't block this signal. gdb tends to do that :-(
    sigprocmask( SIG_UNBLOCK, &act.sa_mask, 0 );

}

void ScopeProcessController::resetHandlers()
{
    if( !ScopeProcessControllerPrivate::handlerSet )
        return;
    ScopeProcessControllerPrivate::handlerSet = false;

    sigset_t mask, omask;
    sigemptyset( &mask );
    sigaddset( &mask, SIGCHLD );
    sigprocmask( SIG_BLOCK, &mask, &omask );

    struct sigaction act;
    sigaction( SIGCHLD, &ScopeProcessControllerPrivate::oldChildHandlerData, &act );
    if (act.sa_handler != theReaper) {
        sigaction( SIGCHLD, &act, 0 );
        ScopeProcessControllerPrivate::handlerSet = true;
    }

    sigprocmask( SIG_SETMASK, &omask, 0 );

    // there should be no problem with SIGPIPE staying SIG_IGN
}

// the pipe is needed to sync the child reaping with our event processing,
// as otherwise there are race conditions, locking requirements, and things
// generally get harder
void ScopeProcessController::theSigCHLDHandler( int arg )
{
    int saved_errno = errno;

    char dummy = 0;
    ssize_t result = ::write( instance()->d_ptr->fd[1], &dummy, 1 );
    if (result < 0) {
        qDebug() << "Write failed with the error code " << result;
    }


    if ( ScopeProcessControllerPrivate::oldChildHandlerData.sa_handler != SIG_IGN &&
         ScopeProcessControllerPrivate::oldChildHandlerData.sa_handler != SIG_DFL ) {
        ScopeProcessControllerPrivate::oldChildHandlerData.sa_handler( arg ); // call the old handler
    }


    errno = saved_errno;
}

int ScopeProcessController::notifierFd() const
{
    const Q_D(ScopeProcessController);
    return d->fd[0];
}

void ScopeProcessController::unscheduleCheck()
{
    Q_D(ScopeProcessController);
    char dummy[16]; // somewhat bigger - just in case several have queued up
    if( ::read( d->fd[0], dummy, sizeof(dummy) ) > 0 )
        d->needcheck = true;
}

void ScopeProcessController::rescheduleCheck()
{
    Q_D(ScopeProcessController);
    if( d->needcheck )
    {
        d->needcheck = false;
        char dummy = 0;
        ssize_t result = ::write( d->fd[1], &dummy, 1 );
        if (result < 0) {
            qDebug() << "Write failed with the error code " << result;
        }

    }
}

void ScopeProcessController::slotDoHousekeeping()
{
    Q_D(ScopeProcessController);
    char dummy[16]; // somewhat bigger - just in case several have queued up
    ssize_t result = ::read( d->fd[0], dummy, sizeof(dummy) );
    if (result < 0) {
        qDebug() << "Write failed with the error code " << result;
    }

    int status;
again:
    QList<ScopeRunner*>::iterator it( d->scopeProcessList.begin() );
    QList<ScopeRunner*>::iterator eit( d->scopeProcessList.end() );
    while( it != eit )
    {
        ScopeRunner *prc = *it;
        if( prc->m_runs && waitpid( prc->m_pid, &status, WNOHANG ) > 0 )
        {
            prc->processHasExited( status );
            // the callback can nuke the whole process list and even 'this'
            if (!instance())
                return;
            goto again;
        }
        ++it;
    }
    QList<int>::iterator uit( d->unixProcessList.begin() );
    QList<int>::iterator ueit( d->unixProcessList.end() );
    while( uit != ueit )
    {
        if( waitpid( *uit, 0, WNOHANG ) > 0 )
        {
            uit = d->unixProcessList.erase( uit );
            deref(); // counterpart to addProcess, can invalidate 'this'
        } else
            ++uit;
    }
}

bool ScopeProcessController::waitForProcessExit( int timeout )
{
    Q_D(ScopeProcessController);
    for(;;)
    {
        struct timeval tv, *tvp;
        if (timeout < 0)
            tvp = 0;
        else
        {
            tv.tv_sec = timeout;
            tv.tv_usec = 0;
            tvp = &tv;
        }

        fd_set fds;
        FD_ZERO( &fds );
        FD_SET( d->fd[0], &fds );

        switch( select( d->fd[0]+1, &fds, 0, 0, tvp ) )
        {
        case -1:
            if( errno == EINTR )
                continue;
            // fall through; should never happen
        case 0:
            return false;
        default:
            slotDoHousekeeping();
            return true;
        }
    }
}

void ScopeProcessController::addScopeRunner( ScopeRunner* p )
{
    Q_D(ScopeProcessController);
    d->scopeProcessList.append( p );
}

void ScopeProcessController::removeScopeRunner( ScopeRunner* p )
{
    Q_D(ScopeProcessController);
    d->scopeProcessList.removeAll( p );
}

void ScopeProcessController::addProcess( int pid )
{
    Q_D(ScopeProcessController);
    d->unixProcessList.append( pid );
    ref(); // make sure we stay around when the ScopeRunner goes away
}
