#include "scoperunner.h"
#include "scopeprocesscontroller.h"
#include "statetracker.h"

#include "kpty.h"

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <errno.h>
#include <assert.h>
#include <fcntl.h>
#include <time.h>
#include <stdlib.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>

#include <QtCore/QMap>
#include <QtCore/QFile>
#include <QtCore/QSocketNotifier>
#include <QtCore/QUuid>
#include <QtDBus/QDBusObjectPath>

class ScopeRunnerPrivate {
public:
    ScopeRunnerPrivate(const QDBusConnection &conn, ScopeRunner *parent) :
        usePty(ScopeRunner::NoCommunication),
        addUtmp(false),
        useShell(false),
        pty(0),
        priority(0),
        q_ptr(parent),
        scopeName(QUuid::createUuid().toString()),
        manager(QSharedPointer<Systemd::Manager>(new Systemd::Manager(conn))),
        tracker(new StateTracker(manager,parent))
    {
    }

    Q_DECLARE_PUBLIC(ScopeRunner)

    /*!
     * \fn static QVariant dbus_list_helper( const T &item )
     */
    template <typename T>
    static QVariant dbus_list_helper( const T &item )
    {
        QList<T> result;
        result << item;
        return QVariant::fromValue(result);
    }

    void createScope( const pid_t &pid )
    {
        Q_Q(ScopeRunner);

        qDebug() << Q_FUNC_INFO << pid;

        UnitPropertyList props;
        foreach( QString key, properties.keys()) {
            props << UnitProperty({key, properties[key]});
        }
        // Set unit environment here from env

        //!!!NOTE!!! PIDs MUST be cast to uint32_t or it won't resolve back to the correct container
        props << UnitProperty({QLatin1String("PIDs"), dbus_list_helper((uint32_t) pid) } );
        props << UnitProperty({QLatin1String("SendSIGHUP"),true});
        props << UnitProperty({QLatin1String("Description"),QLatin1String("this is a test")});

        tracker->track(q->fullScopeName());
        manager->interface<org::freedesktop::systemd1::Manager>()->StartTransientUnit(q->fullScopeName(),
                                                                                      QLatin1String("fail"),
                                                                                      props,
                                                                                      SystemdAuxList());

        QObject::connect(manager->interface<org::freedesktop::systemd1::Manager>(),
                         &org::freedesktop::systemd1::Manager::UnitNew,
                         [=](QString name,QDBusObjectPath){
            QString path =  manager->interface<org::freedesktop::systemd1::Manager>()->GetUnitByPID(pid).value().path();

            if( !path.isEmpty() ){
                qDebug() << Q_FUNC_INFO << name << path;

                q->scopeReady(
                            QSharedPointer<Systemd::Scope>(new Systemd::Scope( path, manager->connection()))
                            );

            };
        });
    }

    ScopeRunner::Communication usePty;
    bool addUtmp : 1;
    bool useShell : 1;

    KPty *pty;

    int priority;
    ScopeRunner *q_ptr;

    QMap<QString,QString> env;
    QMap<QString,QVariant> properties;
    QString wd;
    QByteArray shell;
    QByteArray executable;
    QString scopeName;
    QSharedPointer<Systemd::Manager> manager;
    StateTracker *tracker;
};

ScopeRunner::ScopeRunner(const QDBusConnection &conn, QObject* parent )
    : QObject( parent ),
      m_run_mode(NotifyOnExit),
      m_runs(false),
      m_pid(0),
      m_status(0),
      m_keepPrivs(false),
      m_innot(0),
      m_outnot(0),
      m_errnot(0),
      m_communication(NoCommunication),
      m_input_data(0),
      m_input_sent(0),
      m_input_total(0),
      d_ptr(new ScopeRunnerPrivate(conn,this))
{
    Q_D(ScopeRunner);

    ScopeProcessController::ref();
    ScopeProcessController::instance()->addScopeRunner(this);

    m_out[0] = m_out[1] = -1;
    m_in[0] = m_in[1] = -1;
    m_err[0] = m_err[1] = -1;
}

void ScopeRunner::setEnvironment(const QString &name, const QString &value)
{
    Q_D(ScopeRunner);
    d->env.insert(name, value);
}

void ScopeRunner::setWorkingDirectory(const QString &dir)
{
    Q_D(ScopeRunner);
    d->wd = dir;
}

void ScopeRunner::setupEnvironment()
{
    Q_D(ScopeRunner);
    QMap<QString,QString>::Iterator it;
    for(it = d->env.begin(); it != d->env.end(); ++it)
    {
        setenv(QFile::encodeName(it.key()).data(),
               QFile::encodeName(it.value()).data(), 1);
    }
    if (!d->wd.isEmpty())
    {
        if (-1 == chdir(QFile::encodeName(d->wd).constData())) {
            qDebug() << "Can't change directory: " << strerror(errno);
        }
    }
}

void ScopeRunner::setRunPrivileged(bool keepPrivileges)
{
    m_keepPrivs = keepPrivileges;
}

bool ScopeRunner::runPrivileged() const
{
    return m_keepPrivs;
}

bool ScopeRunner::setPriority(int prio)
{
    Q_D(ScopeRunner);
    if (m_runs) {
        if (setpriority(PRIO_PROCESS, m_pid, prio))
            return false;
    } else {
        if (prio > 19 || prio < (geteuid() ? getpriority(PRIO_PROCESS, 0) : -20))
            return false;
    }
    d->priority = prio;
    return true;
}

QString ScopeRunner::fullScopeName() const
{
    Q_D(const ScopeRunner);

    QString fullScopeNameTemplate = QLatin1Literal("%1.scope");
    return fullScopeNameTemplate.arg(d->scopeName);
}

QString ScopeRunner::scopeName() const
{
    Q_D(const ScopeRunner);
    return d->scopeName;
}

void ScopeRunner::setScopeName(const QString &name)
{

    Q_D(ScopeRunner);
    d->scopeName = name;
}

QSharedPointer<Systemd::Scope> ScopeRunner::scope() const
{
    Q_D(const ScopeRunner);

    QString path =  d->manager->interface<org::freedesktop::systemd1::Manager>()->GetUnitByPID(m_pid).value().path();

    if( !path.isEmpty() ){
        return QSharedPointer<Systemd::Scope>(new Systemd::Scope( path, d->manager->connection()));
    } else {
        return QSharedPointer<Systemd::Scope>();
    }
}

bool ScopeRunner::waitForScopeStarted(int msecs) const
{
    Q_D(const ScopeRunner);

    QEventLoop loop;
    QTimer::singleShot(msecs,&loop,SLOT(quit()));
    connect( d->tracker, &StateTracker::unitStarted, &loop, &QEventLoop::quit);

    if ( !d->tracker->isStarted() ) {
        loop.exec();
    }

    return d->tracker->isStarted();
}

bool ScopeRunner::waitForScopeStopped(int msecs) const
{
    Q_D(const ScopeRunner);

    QEventLoop loop;
    QTimer::singleShot(msecs,&loop,SLOT(quit()));
    connect( d->tracker, &StateTracker::unitStopped, &loop, &QEventLoop::quit);

    if ( d->tracker->isStarted() ) {
        loop.exec();
    }

    return !d->tracker->isStarted();
}

ScopeRunner::~ScopeRunner()
{
    Q_D(ScopeRunner);
    if (m_run_mode != DontCare)
        kill(SIGKILL);
    detach();

    delete d->pty;

    ScopeProcessController::instance()->removeScopeRunner(this);
    ScopeProcessController::deref();
}

void ScopeRunner::detach()
{
    if (m_runs) {
        ScopeProcessController::instance()->addProcess(m_pid);
        m_runs = false;
        m_pid = 0; // close without draining
        commClose(); // Clean up open fd's and socket notifiers.
    }
}

void ScopeRunner::setBinaryExecutable(const char *filename)
{
    Q_D(ScopeRunner);
    d->executable = filename;
}

bool ScopeRunner::start(RunMode runmode, Communication comm)
{
    Q_D(ScopeRunner);
    if (m_runs) {
        qDebug() << "Attempted to start an already running process";
        return false;
    }

    uint n = m_arguments.count();
    if (n == 0) {
        qDebug() << "Attempted to start a process without arguments";
        return false;
    }
    char **arglist;
    QByteArray shellCmd;
    if (d->useShell)
    {
        if (d->shell.isEmpty()) {
            qDebug() << "Invalid shell specified";
            return false;
        }

        for (uint i = 0; i < n; i++) {
            shellCmd += m_arguments[i];
            shellCmd += ' '; // CC: to separate the arguments
        }

        arglist = static_cast<char **>(malloc( 4 * sizeof(char *)));
        arglist[0] = d->shell.data();
        arglist[1] = (char *) "-c";
        arglist[2] = shellCmd.data();
        arglist[3] = 0;
    }
    else
    {
        arglist = static_cast<char **>(malloc( (n + 1) * sizeof(char *)));
        for (uint i = 0; i < n; i++)
            arglist[i] = m_arguments[i].data();
        arglist[n] = 0;
    }

    m_run_mode = runmode;

    if (!setupCommunication(comm))
    {
        qDebug() << "Could not setup Communication!";
        free(arglist);
        return false;;
    }

    // We do this in the parent because if we do it in the child process
    // gdb gets confused when the application runs from gdb.
#ifdef HAVE_INITGROUPS
    struct passwd *pw = geteuid() ? 0 : getpwuid(getuid());
#endif

    int fd[2];
    if (pipe(fd))
        fd[0] = fd[1] = -1; // Pipe failed.. continue

    // we don't use vfork() because
    // - it has unclear semantics and is not standardized
    // - we do way too much magic in the child
    m_pid = fork();
    if (m_pid == 0) {
        // The child process
        close(fd[0]);
        // Closing of fd[1] indicates that the execvp() succeeded!
        fcntl(fd[1], F_SETFD, FD_CLOEXEC);

        if (!commSetupDoneC())
            qDebug() << "Could not finish comm setup in child!";

        // reset all signal handlers
        struct sigaction act;
        sigemptyset(&act.sa_mask);
        act.sa_handler = SIG_DFL;
        act.sa_flags = 0;
        for (int sig = 1; sig < NSIG; sig++)
            sigaction(sig, &act, 0L);

        if (d->priority)
            setpriority(PRIO_PROCESS, 0, d->priority);

        if (!runPrivileged())
        {
            setgid(getgid());
#ifdef HAVE_INITGROUPS
            if (pw)
                initgroups(pw->pw_name, pw->pw_gid);
#endif
            if (geteuid() != getuid())
                setuid(getuid());
            if (geteuid() != getuid())
                _exit(1);
        }

        setupEnvironment();

        if (runmode == DontCare || runmode == OwnGroup)
            setsid();

        const char *executable = arglist[0];
        if (!d->executable.isEmpty())
            executable = d->executable.data();
        execvp(executable, arglist);

        char resultByte = 1;
        ssize_t result = write(fd[1], &resultByte, 1);
        if (result<0) {
            qDebug() << "Write failed with the error code " << result;
        }
        _exit(-1);
    } else if (m_pid == -1) {
        // forking failed

        // commAbort();
        m_pid = 0;
        free(arglist);
        return false;;
    }
    // the parent continues here
    d->createScope(m_pid);

    free(arglist);

    if (!commSetupDoneP())
        qDebug() << "Could not finish comm setup in parent!";

    // Check whether client could be started.
    close(fd[1]);
    for(;;)
    {
        char resultByte;
        int n = ::read(fd[0], &resultByte, 1);
        if (n == 1)
        {
            // exec() failed
            close(fd[0]);
            waitpid(m_pid, 0, 0);
            m_pid = 0;
            commClose();
            return false;
        }
        if (n == -1)
        {
            if (errno == EINTR)
                continue; // Ignore
        }
        break; // success
    }
    close(fd[0]);

    m_runs = true;
    m_input_data = 0; // Discard any data for stdin that might still be there

    return true;
}



bool ScopeRunner::kill(int signo)
{
    if (m_runs && m_pid > 0 && !::kill(m_run_mode == OwnGroup ? -m_pid : m_pid, signo))
        return true;
    return false;
}



bool ScopeRunner::isRunning() const
{
    return m_runs;
}



pid_t ScopeRunner::pid() const
{
    return m_pid;
}


bool ScopeRunner::normalExit() const
{
    return (m_pid != 0) && !m_runs && WIFEXITED(m_status);
}


bool ScopeRunner::signalled() const
{
    return (m_pid != 0) && !m_runs && WIFSIGNALED(m_status);
}


bool ScopeRunner::coreDumped() const
{
    return signalled() && WCOREDUMP(m_status);
}


int ScopeRunner::exitStatus() const
{
    return WEXITSTATUS(m_status);
}


int ScopeRunner::exitSignal() const
{
    return WTERMSIG(m_status);
}


bool ScopeRunner::writeStdin(const QByteArray &buffer)
{
    // if there is still data pending, writing new data
    // to stdout is not allowed (since it could also confuse
    // ScopeRunner ...)
    if (m_input_data != 0)
        return false;

    if (m_communication & Stdin) {
        m_input_data = buffer.constData();
        m_input_sent = 0;
        m_input_total = buffer.length();
        m_innot->setEnabled(true);
        if (m_input_total)
            slotSendData(0);
        return true;
    } else
        return false;
}

void ScopeRunner::suspend()
{
    if (m_outnot)
        m_outnot->setEnabled(false);
}

void ScopeRunner::resume()
{
    if (m_outnot)
        m_outnot->setEnabled(true);
}

bool ScopeRunner::closeStdin()
{
    Q_D(ScopeRunner);
    if (m_communication & Stdin) {
        m_communication = m_communication & ~Stdin;
        delete m_innot;
        m_innot = 0;
        if (!(d->usePty & Stdin))
            close(m_in[1]);
        m_in[1] = -1;
        return true;
    } else
        return false;
}

bool ScopeRunner::closeStdout()
{
    Q_D(ScopeRunner);
    if (m_communication & Stdout) {
        m_communication = m_communication & ~Stdout;
        delete m_outnot;
        m_outnot = 0;
        if (!(d->usePty & Stdout))
            close(m_out[0]);
        m_out[0] = -1;
        return true;
    } else
        return false;
}

bool ScopeRunner::closeStderr()
{
    Q_D(ScopeRunner);
    if (m_communication & Stderr) {
        m_communication = m_communication & ~Stderr;
        delete m_errnot;
        m_errnot = 0;
        if (!(d->usePty & Stderr))
            close(m_err[0]);
        m_err[0] = -1;
        return true;
    } else
        return false;
}

bool ScopeRunner::closePty()
{
    Q_D(ScopeRunner);
    if (d->pty && d->pty->masterFd() >= 0) {
        if (d->addUtmp)
            d->pty->logout();
        d->pty->close();
        return true;
    } else
        return false;
}

void ScopeRunner::closeAll()
{
    closeStdin();
    closeStdout();
    closeStderr();
    closePty();
}

QList<QByteArray> ScopeRunner::arguments() const
{
    return m_arguments;
}

void ScopeRunner::setArguments(const QList<QByteArray> &args)
{
    m_arguments = args;
}

/////////////////////////////
// protected slots         //
/////////////////////////////



void ScopeRunner::slotChildOutput(int fdno)
{
    if (!childOutput(fdno))
        closeStdout();
}


void ScopeRunner::slotChildError(int fdno)
{
    if (!childError(fdno))
        closeStderr();
}


void ScopeRunner::slotSendData(int)
{
    if (m_input_sent == m_input_total) {
        m_innot->setEnabled(false);
        m_input_data = 0;
        emit wroteStdin();
    } else {
        int result = ::write(m_in[1], m_input_data+m_input_sent, m_input_total-m_input_sent);
        if (result >= 0)
        {
            m_input_sent += result;
        }
        else if ((errno != EAGAIN) && (errno != EINTR))
        {
            qDebug() << "Error writing to stdin of child process";
            closeStdin();
        }
    }
}

void ScopeRunner::setUseShell(bool useShell, const char *shell)
{
    Q_D(ScopeRunner);
    d->useShell = useShell;
    if (shell && *shell)
        d->shell = shell;
    else
        d->shell = "/bin/sh";
}

void ScopeRunner::setUsePty(Communication usePty, bool addUtmp)
{
    Q_D(ScopeRunner);
    d->usePty = usePty;
    d->addUtmp = addUtmp;
    if (usePty) {
        if (!d->pty)
            d->pty = new KPty;
    } else {
        delete d->pty;
        d->pty = 0;
    }
}

QString ScopeRunner::quote(const QString &arg)
{
    QChar q('\'');
    return QString(arg).replace(q, "'\\''").prepend(q).append(q);
}


//////////////////////////////
// private member functions //
//////////////////////////////


void ScopeRunner::processHasExited(int state)
{
    // only successfully run NotifyOnExit processes ever get here

    m_status = state;
    m_runs = false; // do this before commClose, so it knows we're dead

    commClose(); // cleanup communication sockets

    if (m_run_mode != DontCare)
        emit processExited();
}



int ScopeRunner::childOutput(int fdno)
{
    if (m_communication & NoRead) {
        int len = -1;
        emit receivedStdoutFD(fdno, len);
        errno = 0; // Make sure errno doesn't read "EAGAIN"
        return len;
    }
    else
    {
        char buffer[1025];
        int len;

        len = ::read(fdno, buffer, 1024);

        if (len > 0) {
            buffer[len] = 0; // Just in case.
            emit receivedStdout(QByteArray( buffer, len));
        }
        return len;
    }
}

int ScopeRunner::childError(int fdno)
{
    char buffer[1025];
    int len;

    len = ::read(fdno, buffer, 1024);

    if (len > 0) {
        buffer[len] = 0; // Just in case.
        emit receivedStderr(QByteArray( buffer, len));
    }
    return len;
}


int ScopeRunner::setupCommunication(Communication comm)
{
    Q_D(ScopeRunner);

    // PTY stuff //
    if (d->usePty)
    {
        // cannot communicate on both stderr and stdout if they are both on the pty
        if (!(~(comm & d->usePty) & (Stdout | Stderr))) {
            qWarning() << "Invalid usePty/communication combination (" << d->usePty << "/" << comm << ")";
            return 0;
        }
        if (!d->pty->open())
            return 0;

        int rcomm = comm & d->usePty;
        int mfd = d->pty->masterFd();
        if (rcomm & Stdin)
            m_in[1] = mfd;
        if (rcomm & Stdout)
            m_out[0] = mfd;
        if (rcomm & Stderr)
            m_err[0] = mfd;
    }

    m_communication = comm;

    comm = comm & ~d->usePty;
    if (comm & Stdin) {
        if (socketpair(AF_UNIX, SOCK_STREAM, 0, m_in))
            goto fail0;
        fcntl(m_in[0], F_SETFD, FD_CLOEXEC);
        fcntl(m_in[1], F_SETFD, FD_CLOEXEC);
    }
    if (comm & Stdout) {
        if (socketpair(AF_UNIX, SOCK_STREAM, 0, m_out))
            goto fail1;
        fcntl(m_out[0], F_SETFD, FD_CLOEXEC);
        fcntl(m_out[1], F_SETFD, FD_CLOEXEC);
    }
    if (comm & Stderr) {
        if (socketpair(AF_UNIX, SOCK_STREAM, 0, m_err))
            goto fail2;
        fcntl(m_err[0], F_SETFD, FD_CLOEXEC);
        fcntl(m_err[1], F_SETFD, FD_CLOEXEC);
    }
    return 1; // Ok
fail2:
    if (comm & Stdout)
    {
        close(m_out[0]);
        close(m_out[1]);
        m_out[0] = m_out[1] = -1;
    }
fail1:
    if (comm & Stdin)
    {
        close(m_in[0]);
        close(m_in[1]);
        m_in[0] = m_in[1] = -1;
    }
fail0:
    m_communication = NoCommunication;
    return 0; // Error
}



int ScopeRunner::commSetupDoneP()
{
    Q_D(ScopeRunner);
    int rcomm = m_communication & ~d->usePty;
    if (rcomm & Stdin)
        close(m_in[0]);
    if (rcomm & Stdout)
        close(m_out[1]);
    if (rcomm & Stderr)
        close(m_err[1]);
    m_in[0] = m_out[1] = m_err[1] = -1;

    // Don't create socket notifiers if no interactive comm is to be expected
    if (m_run_mode != NotifyOnExit && m_run_mode != OwnGroup)
        return 1;

    if (m_communication & Stdin) {
        fcntl(m_in[1], F_SETFL, O_NONBLOCK | fcntl(m_in[1], F_GETFL));
        m_innot =  new QSocketNotifier(m_in[1], QSocketNotifier::Write, this);
        Q_CHECK_PTR(m_innot);
        m_innot->setEnabled(false); // will be enabled when data has to be sent
        QObject::connect(m_innot, SIGNAL(activated(int)),
                         this, SLOT(slotSendData(int)));
    }

    if (m_communication & Stdout) {
        m_outnot = new QSocketNotifier(m_out[0], QSocketNotifier::Read, this);
        Q_CHECK_PTR(m_outnot);
        QObject::connect(m_outnot, SIGNAL(activated(int)),
                         this, SLOT(slotChildOutput(int)));
        if (m_communication & NoRead)
            suspend();
    }

    if (m_communication & Stderr) {
        m_errnot = new QSocketNotifier(m_err[0], QSocketNotifier::Read, this );
        Q_CHECK_PTR(m_errnot);
        QObject::connect(m_errnot, SIGNAL(activated(int)),
                         this, SLOT(slotChildError(int)));
    }

    return 1;
}



int ScopeRunner::commSetupDoneC()
{
    Q_D(ScopeRunner);
    int ok = 1;
    if (d->usePty & Stdin) {
        if (dup2(d->pty->slaveFd(), STDIN_FILENO) < 0) ok = 0;
    } else if (m_communication & Stdin) {
        if (dup2(m_in[0], STDIN_FILENO) < 0) ok = 0;
    } else {
        int null_fd = open( "/dev/null", O_RDONLY );
        if (dup2( null_fd, STDIN_FILENO ) < 0) ok = 0;
        close( null_fd );
    }
    struct linger so;
    memset(&so, 0, sizeof(so));
    if (d->usePty & Stdout) {
        if (dup2(d->pty->slaveFd(), STDOUT_FILENO) < 0) ok = 0;
    } else if (m_communication & Stdout) {
        if (dup2(m_out[1], STDOUT_FILENO) < 0 ||
                setsockopt(m_out[1], SOL_SOCKET, SO_LINGER, (char *)&so, sizeof(so)))
            ok = 0;
        if (m_communication & MergedStderr) {
            if (dup2(m_out[1], STDERR_FILENO) < 0)
                ok = 0;
        }
    }
    if (d->usePty & Stderr) {
        if (dup2(d->pty->slaveFd(), STDERR_FILENO) < 0) ok = 0;
    } else if (m_communication & Stderr) {
        if (dup2(m_err[1], STDERR_FILENO) < 0 ||
                setsockopt(m_err[1], SOL_SOCKET, SO_LINGER, (char *)&so, sizeof(so)))
            ok = 0;
    }

    // don't even think about closing all open fds here or anywhere else

    // PTY stuff //
    if (d->usePty) {
        d->pty->setCTty();
        if (d->addUtmp)
            d->pty->login(getenv("USER"), getenv("DISPLAY"));
    }

    return ok;
}



void ScopeRunner::commClose()
{
    closeStdin();

    if (m_pid) { // detached, failed, and killed processes have no output. basta. :)
        // If both channels are being read we need to make sure that one socket
        // buffer doesn't fill up whilst we are waiting for data on the other
        // (causing a deadlock). Hence we need to use select.

        int notfd = ScopeProcessController::instance()->notifierFd();

        while ((m_communication & (Stdout | Stderr)) || m_runs) {
            fd_set rfds;
            FD_ZERO(&rfds);
            struct timeval timeout, *p_timeout;

            int max_fd = 0;
            if (m_communication & Stdout) {
                FD_SET(m_out[0], &rfds);
                max_fd = m_out[0];
            }
            if (m_communication & Stderr) {
                FD_SET(m_err[0], &rfds);
                if (m_err[0] > max_fd)
                    max_fd = m_err[0];
            }
            if (m_runs) {
                FD_SET(notfd, &rfds);
                if (notfd > max_fd)
                    max_fd = notfd;
                // If the process is still running we block until we
                // receive data or the process exits.
                p_timeout = 0; // no timeout
            } else {
                // If the process has already exited, we only check
                // the available data, we don't wait for more.
                timeout.tv_sec = timeout.tv_usec = 0; // timeout immediately
                p_timeout = &timeout;
            }

            int fds_ready = select(max_fd+1, &rfds, 0, 0, p_timeout);
            if (fds_ready < 0) {
                if (errno == EINTR)
                    continue;
                break;
            } else if (!fds_ready)
                break;

            if ((m_communication & Stdout) && FD_ISSET(m_out[0], &rfds))
                slotChildOutput(m_out[0]);

            if ((m_communication & Stderr) && FD_ISSET(m_err[0], &rfds))
                slotChildError(m_err[0]);

            if (m_runs && FD_ISSET(notfd, &rfds)) {
                m_runs = false; // hack: signal potential exit
                return; // don't close anything, we will be called again
            }
        }
    }

    closeStdout();
    closeStderr();

    closePty();
}
