/*
 * qtsystemd - A Qt5 wrapper to the systemd/logind/machine API.
 * Copyright (C) 2015 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "statetracker.h"
#include <QDebug>
#include <QTimer>

/*!
 * \class StateTracker
 * \brief Tracks the current state of a specified Systemd Unit
 * \inmodule QtSystemd
 */
StateTracker::StateTracker(const QSharedPointer<Systemd::Manager> &mgr, QObject *parent) :
    QObject(parent), m_manager(mgr), m_ignoreAddRemove(false)
{
    m_manager->interface<org::freedesktop::systemd1::Manager>()->Subscribe();
    connect( m_manager->interface<org::freedesktop::systemd1::Manager>(), SIGNAL(UnitNew(QString,QDBusObjectPath)),
             this, SLOT(onUnitNew(QString,QDBusObjectPath)));
    connect( m_manager->interface<org::freedesktop::systemd1::Manager>(), SIGNAL(UnitRemoved(QString,QDBusObjectPath)),
             this, SLOT(onUnitRemoved(QString,QDBusObjectPath)));
}

StateTracker::~StateTracker()
{
    m_manager->interface<org::freedesktop::systemd1::Manager>()->Unsubscribe();
}

void StateTracker::configureUnit(const QString &unit, bool autoload)
{

    QDBusReply<QDBusObjectPath> path = m_manager->interface<org::freedesktop::systemd1::Manager>()->GetUnit(unit);
    if ( !path.isValid() && autoload ) {
        path = m_manager->interface<org::freedesktop::systemd1::Manager>()->LoadUnit(unit);
    }

    if ( path.isValid() ) {
        qDebug() << Q_FUNC_INFO << unit << autoload;

        m_unit = QSharedPointer<Systemd::Unit>( new Systemd::Unit(path.value().path(), m_manager->connection()));

        connect(m_unit->interface<org::freedesktop::DBus::Properties>(), SIGNAL(PropertiesChanged(QString,QVariantMap,QStringList)),
                this, SLOT(onPropertiesChanged(QString,QVariantMap,QStringList)));
    }
}

void StateTracker::track(const QString &unit, bool autoload)
{
    m_unitName = unit;
    configureUnit(unit,autoload);
}

void StateTracker::start()
{
    if ( isValid() ) {
        m_unit->interface<org::freedesktop::systemd1::Unit>()->Start(QLatin1String("fail"));
    } else if ( !isValid() && m_unitName.endsWith(QLatin1String(".target"))) {
        configureUnit(m_unitName, true);
        m_unit->interface<org::freedesktop::systemd1::Unit>()->Start(QLatin1String("fail"));
    }
}

void StateTracker::stop()
{
    if ( isValid() ) {
        m_unit->interface<org::freedesktop::systemd1::Unit>()->Stop(QLatin1String("fail"));
    }
}

bool StateTracker::isValid( ) const
{
    return !(m_unit.isNull() || m_unit->path().isEmpty());
}

bool StateTracker::isStarted() const
{
    if( isValid() ) {
        qDebug() << Q_FUNC_INFO << m_unit->interface<org::freedesktop::systemd1::Unit>()->activeState();
        return (m_unit->interface<org::freedesktop::systemd1::Unit>()->activeState() == QLatin1String("active"));
    } else {
        return false;
    }
}

QString StateTracker::unitPath() const
{
    if ( isValid() ) {
        return m_unit->path();
    } else {
        return QString();
    }
}

void StateTracker::onPropertiesChanged(const QString &interface, const QVariantMap &changed_properties, const QStringList &invalidated_properties)
{
    if ( isValid() ) {
        qDebug() << Q_FUNC_INFO
                 << interface
                 << changed_properties
                 << invalidated_properties;
    }
    if ( isValid() && changed_properties.contains(QLatin1String("ActiveState")) &&
         changed_properties[QLatin1String("ActiveState")] == QLatin1String("inactive")) {
        emit unitStopped();
    }

    if ( isValid() && changed_properties.contains(QLatin1String("ActiveState")) &&
         changed_properties[QLatin1String("ActiveState")] == QLatin1String("active")) {
        emit unitStarted();
    }
}

void StateTracker::onUnitNew(const QString &unitName, const QDBusObjectPath &path)
{
    // The ignore functions work around a wart in the systemd dbus interface.
    // Requesting the properties of an unloaded unit will cause systemd to send a
    // pair of UnitNew/UnitRemoved signals.  Because we need to get a unit's
    // properties on UnitNew (as that's the only indication of a new unit coming up
    // for the first time), we would enter an infinite loop if we did not attempt
    // to detect and ignore these spurious signals.  The signal themselves are
    // indistinguishable from relevant ones, so we (somewhat hackishly) ignore an
    // unloaded unit's signals for a short time after requesting its properties.
    // This means that we will miss e.g. a transient unit being restarted
    // *immediately* upon failure and also a transient unit being started
    // immediately after requesting its status (with systemctl status, for example,
    // because this causes a UnitNew signal to be sent which then causes us to fetch
    // the properties).

    qDebug() << Q_FUNC_INFO
             << unitName
             << path.path();

    if( unitName == m_unitName && !m_ignoreAddRemove ) {
        m_ignoreAddRemove = true;
        QTimer::singleShot(10, [this]() {
            m_ignoreAddRemove = false;
        });

        if (!isValid()) {
            configureUnit(unitName);
        }

        if ( isValid() ) {
            qDebug() << Q_FUNC_INFO
                     << "watch"
                     << unitName
                     << path.path();

            emit unitLoaded();
        }

        if ( isStarted() ) {
            emit unitStarted();
        }
    }
}

void StateTracker::onUnitRemoved(const QString &unitName, const QDBusObjectPath &path)
{

    // The ignore functions work around a wart in the systemd dbus interface.
    // Requesting the properties of an unloaded unit will cause systemd to send a
    // pair of UnitNew/UnitRemoved signals.  Because we need to get a unit's
    // properties on UnitNew (as that's the only indication of a new unit coming up
    // for the first time), we would enter an infinite loop if we did not attempt
    // to detect and ignore these spurious signals.  The signal themselves are
    // indistinguishable from relevant ones, so we (somewhat hackishly) ignore an
    // unloaded unit's signals for a short time after requesting its properties.
    // This means that we will miss e.g. a transient unit being restarted
    // *immediately* upon failure and also a transient unit being started
    // immediately after requesting its status (with systemctl status, for example,
    // because this causes a UnitNew signal to be sent which then causes us to fetch
    // the properties).

    qDebug() << Q_FUNC_INFO
             << unitName
             << path.path();

    if( isValid() && unitName == m_unitName && !m_ignoreAddRemove ) {
        m_ignoreAddRemove = true;
        QTimer::singleShot(10, [this]() {
            m_ignoreAddRemove = false;
        });
        m_unit.clear();

        qDebug() << Q_FUNC_INFO
                 << "unwatch"
                 << unitName
                 << path.path();

        emit unitRemoved();

    }
}

