/*
 * qtsystemd - A Qt5 wrapper to the systemd/logind/machine API.
 * Copyright (C) 2015 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STATETRACKER_H
#define STATETRACKER_H

#include <QObject>
#include <QSharedPointer>
#include <systemd/manager.h>
#include <systemd/unit.h>

class StateTracker : public QObject
{
    Q_OBJECT
public:
    /*!
     * \brief StateTracker
     * \param mgr - The Manager interface to notify of changes to the unit before and after its lifetime
     * \param parent - QObject parent
     */
    explicit StateTracker(const QSharedPointer<Systemd::Manager> &mgr, QObject *parent = 0);
    ~StateTracker();

    /*!
     * \brief Returns if the Unit itself is valid for use.
     */
    bool isValid() const;

    bool isStarted() const;

    /*!
     * \brief unitPath
     */
    QString unitPath() const;

    /*!
     * \brief Returns an instance to the Unit. This lifetime is outside of the actual DBus instance.  The caller
     * is responsible for the correct type T passed into the method.
     */
    template< typename T>
    QSharedPointer<T> unit() const
    {
        if ( isValid() ) {
            return QSharedPointer<T>( new T( unitPath(),
                                             m_manager->connection()));
        } else {
            return QSharedPointer<T>();
        }
    }

signals:
    void unitStarted();
    void unitStopped();
    void unitLoaded();
    void unitRemoved();

public slots:
    /*!
     * \brief Start tracking a unit.
     * NOTE: if a Target is tracked it will implicitly start when its
     * tracked. This is because the Unit needs to be Loaded before its instance can exist in Systemd.
     * Other units must be started using the StateTracker::start() method.
     * \param unit - The systemd unit name. aka: dbus.service, multiuser.target
     */
    void track(const QString& unit, bool autoload = false);
    void start();
    /*!
     * \brief stop the unit
     * NOTE: Targets implicitly unload when they are stopped.
     */
    void stop();

private slots:
    void onPropertiesChanged(const QString &interface, const QVariantMap &changed_properties, const QStringList &invalidated_properties);
    void onUnitNew(const QString &unitName, const QDBusObjectPath &path);
    void onUnitRemoved(const QString &unitName, const QDBusObjectPath &path);

private:
    void configureUnit(const QString &unit, bool autoload = false);
    QSharedPointer<Systemd::Unit> m_unit;
    QString m_unitName;
    QSharedPointer<Systemd::Manager> m_manager;
    bool m_ignoreAddRemove;
};

#endif // STATETRACKER_H
