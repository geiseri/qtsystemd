#include "systemdbus.h"

#include <QtGlobal>

QDBusConnection SystemdBus::systemBus()
{
    return QDBusConnection::systemBus();
}

QDBusConnection SystemdBus::userBus( const QString &name)
{
    QByteArray bus = QByteArray("unix:path=") + qgetenv("XDG_RUNTIME_DIR") + QByteArray("/bus");
    return QDBusConnection::connectToBus(bus, name);
}
