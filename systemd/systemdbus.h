#ifndef SYSTEMDBUS_H
#define SYSTEMDBUS_H

#include <QDBusConnection>

class SystemdBus {
public:
    static QDBusConnection systemBus();
    static QDBusConnection userBus(const QString &name = QLatin1String("user"));
};

#endif // SYSTEMDBUS_H
